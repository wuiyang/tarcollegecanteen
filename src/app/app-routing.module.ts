// Angular import
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Project's Angular Module/Component import
import { AboutusComponent } from './page/aboutus/aboutus.component';
import { AccountInfoComponent } from './page/account/account-info/account-info.component';
import { ConsumptionComponent } from './page/orderlist/consumption/consumption.component';
import { CreditPointComponent } from './page/account/credit/credit-point/credit-point.component';
import { CreditTopUpComponent } from './page/operator/credit-top-up/credit-top-up.component';
import { FeedbackListComponent } from './page/feedback/feedback-list/feedback-list.component';
import { LoginComponent } from './page/account/login/login.component';
import { LogoutComponent } from './page/account/logout/logout.component';
import { MainpageComponent } from './page/mainpage/mainpage.component';
import { MenuComponent } from './page/order/menu/menu.component';
import { NotFoundComponent } from './page/not-found/not-found.component';
import { OrderHistoryComponent } from './page/orderlist/order-history/order-history.component';
import { VendorCouponVerifyComponent } from './page/vendor/vendor-coupon-verify/vendor-coupon-verify.component';
import { VendorMealComponent } from './page/vendor/vendor-meal/vendor-meal.component';
import { VendorMenuComponent } from './page/vendor/vendor-menu/vendor-menu.component';
import { VendorOrderComponent } from './page/vendor/vendor-order/vendor-order.component';

// Project's Angular Service import
import { AnonymousUserGuard } from './guard/anonymous-user.guard';
import { CustomerGuard } from './guard/customer.guard';
import { RegisteredGuard } from './guard/registered.guard';
import { VendorGuard } from './guard/vendor.guard';
import { OperatorGuard } from './guard/operator.guard';

const routes: Routes = [
  { path: '', data: { title: 'Main Menu' }, component: MainpageComponent },
  { path: 'login', component: LoginComponent, data: { type: 0, title: 'Login'}, canActivate: [AnonymousUserGuard] },
  { path: 'register', component: LoginComponent, data: { type: 1, title: 'Register' }, canActivate: [AnonymousUserGuard] },
  { path: 'forgetPassword', component: LoginComponent, data: { type: 2, title: 'Forget Password' }, canActivate: [AnonymousUserGuard] },
  { path: 'logout', component: LogoutComponent, data: { title: 'Logout' }, canActivate: [RegisteredGuard] },
  { path: 'order', component: MenuComponent, data: { title: 'Order Menu' }, canActivate: [CustomerGuard] },
  { path: 'orderHistory', component: OrderHistoryComponent, data: { title: 'Order History' }, canActivate: [CustomerGuard] },
  { path: 'consumption', component: ConsumptionComponent, data: { title: 'Consumption Report' }, canActivate: [CustomerGuard] },
  { path: 'vendor/meal', component: VendorMealComponent, data: { title: 'Meal Management' }, canActivate: [VendorGuard] },
  { path: 'vendor/menu', component: VendorMenuComponent, data: { title: 'Menu Management' }, canActivate: [VendorGuard] },
  { path: 'vendor/order', component: VendorOrderComponent, data: { title: 'Order List' }, canActivate: [VendorGuard] },
  { path: 'vendor/verify', component: VendorCouponVerifyComponent, data: { title: 'Verify Coupon' }, canActivate: [VendorGuard] },
  { path: 'topup', component: CreditTopUpComponent, data: { title: 'Topup' }, canActivate: [OperatorGuard] },
  { path: 'user/credit', component: CreditPointComponent, data: { title: 'Credit History' }, canActivate: [RegisteredGuard] },
  { path: 'accountInfo', component: AccountInfoComponent, data: { title: 'Account Info' }, canActivate: [RegisteredGuard] },
  { path: 'feedback', component: FeedbackListComponent, data: { title: 'Feedback' }, canActivate: [RegisteredGuard] },
  { path: 'aboutus', component: AboutusComponent, data: { title: 'About Us' } },
  { path: 'notfound', component: NotFoundComponent, data: { title: 'Not Found' } },
  { path: '**', redirectTo: '/notfound' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
