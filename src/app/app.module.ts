// Angular import
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule, MatNativeDateModule, MatTableModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgModule } from '@angular/core';

// Other module import
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

// Project's Angular component import
import { AboutusComponent } from './page/aboutus/aboutus.component';
import { AccountInfoComponent } from './page/account/account-info/account-info.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfirmComponent } from './page/dialog/confirm/confirm.component';
import { ConsumptionComponent } from './page/orderlist/consumption/consumption.component';
import { CouponComponent } from './page/dialog/coupon/coupon.component';
import { CreditExchangeComponent } from './page/dialog/credit-exchange/credit-exchange.component';
import { CreditPointComponent } from './page/account/credit/credit-point/credit-point.component';
import { CreditTopUpComponent } from './page/operator/credit-top-up/credit-top-up.component';
import { FeedbackFormComponent } from './page/dialog/feedback-form/feedback-form.component';
import { FeedbackListComponent } from './page/feedback/feedback-list/feedback-list.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { LoginComponent } from './page/account/login/login.component';
import { LogoutComponent } from './page/account/logout/logout.component';
import { MainpageComponent } from './page/mainpage/mainpage.component';
import { MealComponent } from './page/order/meal/meal.component';
import { MenuComponent } from './page/order/menu/menu.component';
import { NewTransactionComponent } from './page/dialog/new-transaction/new-transaction.component';
import { NotFoundComponent } from './page/not-found/not-found.component';
import { OrderComponent } from './page/order/order/order.component';
import { OrderHistoryComponent } from './page/orderlist/order-history/order-history.component';
import { OrderHistoryDetailComponent } from './page/orderlist/order-history-detail/order-history-detail.component';
import { OrderNewComponent } from './page/dialog/order-new/order-new.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { VendorCouponVerifyComponent } from './page/vendor/vendor-coupon-verify/vendor-coupon-verify.component';
import { VendorInfoComponent } from './page/vendor/vendor-info/vendor-info.component';
import { VendorMealComponent } from './page/vendor/vendor-meal/vendor-meal.component';
import { VendorMealDetailComponent } from './page/vendor/vendor-meal-detail/vendor-meal-detail.component';
import { VendorMenuComponent } from './page/vendor/vendor-menu/vendor-menu.component';
import { VendorMenuMealDetailComponent } from './page/vendor/vendor-menu-meal-detail/vendor-menu-meal-detail.component';
import { VendorMenuNewMealComponent } from './page/dialog/vendor-menu-new-meal/vendor-menu-new-meal.component';
import { VendorOrderComponent } from './page/vendor/vendor-order/vendor-order.component';
import { VendorOrderDetailComponent } from './page/vendor/vendor-order-detail/vendor-order-detail.component';
import { VendorOrderIngredientComponent } from './page/vendor/vendor-order-ingredient/vendor-order-ingredient.component';
import { VendorOrderSummaryComponent } from './page/vendor/vendor-order-summary/vendor-order-summary.component';

// Project's Angular services import
import { AuthUserService } from './services/auth-user.service';
import { ConnectionService } from './services/connection.service';
import { CreditPointService } from './services/user/credit-point.service';
import { LoginService } from './services/login.service';
import { MenuVendorListService } from './services/menu/menu-vendor-list.service';
import { OrderService } from './services/order/order.service';
import { PictureService } from './services/user/picture.service';
import { ScrollToBottomService } from './services/page/scroll-to-bottom.service';
import { VendorInfoService } from './services/vendor/vendor-info.service';
import { VendorMealService } from './services/vendor/vendor-meal.service';
import { VendorMenuService } from './services/vendor/vendor-menu.service';
import { VendorOrderService } from './services/vendor/vendor-order.service';

@NgModule({
  declarations: [
    AboutusComponent,
    AccountInfoComponent,
    AppComponent,
    ConfirmComponent,
    ConsumptionComponent,
    CouponComponent,
    CreditExchangeComponent,
    CreditPointComponent,
    CreditTopUpComponent,
    FeedbackFormComponent,
    FeedbackListComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    LogoutComponent,
    MainpageComponent,
    MealComponent,
    MenuComponent,
    NewTransactionComponent,
    NotFoundComponent,
    OrderComponent,
    OrderHistoryComponent,
    OrderHistoryDetailComponent,
    OrderNewComponent,
    SidebarComponent,
    VendorCouponVerifyComponent,
    VendorInfoComponent,
    VendorMealComponent,
    VendorMealDetailComponent,
    VendorMenuComponent,
    VendorMenuMealDetailComponent,
    VendorMenuNewMealComponent,
    VendorOrderComponent,
    VendorOrderDetailComponent,
    VendorOrderIngredientComponent,
    VendorOrderSummaryComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CdkTableModule,
    FormsModule,
    HttpClientModule,
    LayoutModule,
    MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    NgxQRCodeModule,
    ReactiveFormsModule,
    ZXingScannerModule
  ],
  providers: [
    AuthUserService,
    ConnectionService,
    CreditPointService,
    LoginService,
    MenuVendorListService,
    OrderService,
    PictureService,
    ScrollToBottomService,
    VendorInfoService,
    VendorMealService,
    VendorMenuService,
    VendorOrderService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmComponent,
    CreditExchangeComponent,
    CouponComponent,
    FeedbackFormComponent,
    NewTransactionComponent,
    OrderNewComponent,
    VendorMenuNewMealComponent
  ]
})
export class AppModule { }
