import { Component, OnInit, Input } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { AuthUserService } from 'src/app/services/auth-user.service';
import { CreditPointService } from 'src/app/services/user/credit-point.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ScrollToBottomService } from 'src/app/services/page/scroll-to-bottom.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() SideNav: MatSidenav;
  appName = 'TARUC Canteen Apps';
  username: string;
  link: string;
  needTwoRow = false;
  hideTwoRow = false;

  constructor(private auth: AuthUserService, public creditService: CreditPointService, private breakpointObserver: BreakpointObserver,
              private scrollService: ScrollToBottomService) { }

  ngOnInit() {
    this.hideTwoRow = this.scrollService.getSavedHeight();
    this.scrollService.adjustHeight(this.hideTwoRow);
    this.breakpointObserver.observe('(max-width:  767px)').subscribe(isLessThan => {
      this.needTwoRow = isLessThan.matches;
    });
    this.auth.isLoggedInSub.subscribe( value => { this.updateText(value); });
  }
  updateText(value: boolean) {
    this.username = this.auth.username || 'Login';
    this.link = value ? '/accountInfo' : '/login';
  }
  updatHide() {
    this.hideTwoRow = !this.hideTwoRow;
    this.scrollService.adjustHeight(this.hideTwoRow);
  }
}
