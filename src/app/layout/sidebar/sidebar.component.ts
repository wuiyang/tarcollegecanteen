import { Component, OnInit, Input } from '@angular/core';
import { MatSidenav, MatDialog } from '@angular/material';
import { AuthUserService } from 'src/app/services/auth-user.service';
import { FeedbackFormComponent } from 'src/app/page/dialog/feedback-form/feedback-form.component';
import { Feedback } from 'src/app/models/feedback.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() SideNav: MatSidenav;
  isLogin: boolean;
  loginoutText: string;
  loginoutUrl: string;
  loginoutIcon: string;
  authLevel: number;
  readonly CUSTOMER: number = AuthUserService.CUSTOMER;
  readonly VENDOR: number = AuthUserService.VENDOR;
  readonly OPERATOR: number = AuthUserService.OPERATOR;

  constructor(private dialog: MatDialog, private auth: AuthUserService) { this.updateText(this.auth.isLoggedIn); }

  ngOnInit() {
    this.auth.isLoggedInSub.subscribe( value => { this.updateText(value); this.authLevel = this.auth.accessLevel; });
  }

  updateText(value: boolean) {
    this.isLogin = value;
    this.loginoutText = value ? 'Log out' : 'Login';
    this.loginoutUrl = value ? 'logout' : 'login';
    this.loginoutIcon = value ? 'exit_to_app' : 'account_circle';
  }
}
