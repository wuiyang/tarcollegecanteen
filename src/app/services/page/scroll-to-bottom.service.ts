import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollToBottomService {
  private scrollToBottomEmitter: EventEmitter<boolean>;
  private adjustHeightEmitter: EventEmitter<boolean>;

  constructor() { }

  get scrollEmitter() {
    if (this.scrollToBottomEmitter == null) {
      this.scrollToBottomEmitter = new EventEmitter<boolean>();
    }
    return this.scrollToBottomEmitter;
  }

  get heightEmitter() {
    if (this.adjustHeightEmitter == null) {
      this.adjustHeightEmitter = new EventEmitter<boolean>();
    }
    return this.adjustHeightEmitter;
  }

  scrollToBottom() {
    this.scrollToBottomEmitter.emit(true);
  }
  adjustHeight(hasHide: boolean) {
    this.adjustHeightEmitter.emit(hasHide);
    document.cookie = 'scroll=' + hasHide + '; expires=2030-01-31T16:00:00.000Z; path=/';
  }

  getSavedHeight(): boolean {
    return this.getCookie('scroll') === 'true';
  }

  private getCookie(name: string) {
    const value = '; ' + document.cookie;
    const parts = value.split('; ' + name + '=');
    if (parts.length === 2) { return parts.pop().split(';').shift(); }
  }
}
