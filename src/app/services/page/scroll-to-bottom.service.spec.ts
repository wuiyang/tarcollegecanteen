import { TestBed } from '@angular/core/testing';

import { ScrollToBottomService } from './scroll-to-bottom.service';

describe('ScrollToBottomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScrollToBottomService = TestBed.get(ScrollToBottomService);
    expect(service).toBeTruthy();
  });
});
