import { TestBed } from '@angular/core/testing';

import { MenuVendorListService } from './menu-vendor-list.service';

describe('MenuVendorListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuVendorListService = TestBed.get(MenuVendorListService);
    expect(service).toBeTruthy();
  });
});
