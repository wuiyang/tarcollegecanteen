import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConnectionService } from '../connection.service';
import { Url } from 'src/app/models/geturl.model';
import { map } from 'rxjs/operators';
import { Vendor } from 'src/app/models/vendor.model';
import { Menu } from 'src/app/models/menu.model';
import { Meal } from 'src/app/models/meal.model';

@Injectable({
  providedIn: 'root'
})
export class MenuVendorListService {

  constructor(private connection: ConnectionService) { }

  getVendorList(): Observable<any> {
    return this.connection.get(Url.menuRetriveUrl).pipe(
      map(data => {
        if (data.code === 2) {
          const vendorList = [];
          data.data.forEach(vendorData => {
            vendorList.push(new Vendor(vendorData.id, vendorData.name, vendorData.description, null, vendorData.pictureUrl));
          });
          return vendorList;
        } else {
          return [];
        }
      })
    );
  }

  getMenuDetail(vendor: Vendor, date: Date, isLunch?: boolean): Observable<any> {
    return this.connection.get(Url.menuRetriveUrl + '/' + vendor.id + '/' + date.toDateString() +
                               '/' + (isLunch ? 'lunch' : 'breakfast')).pipe(
      map(data => {
        if (data.code === 2) {
          const menu = new Menu(data.data.id, vendor, new Date(data.data.date), data.data.isLunch);
          data.data.meals.forEach(mealData => {
            menu.meals.push(new Meal(mealData.id, mealData.name, mealData.description, mealData.price,
                                     null, mealData.pictureUrl, null, mealData.calories));
          });
          return menu;
        } else {
          return new Menu(null, null, date, isLunch || false);
        }
      })
    );
  }
}
