import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthUserService {
  static readonly ANONYMOUS = -1;
  static readonly CUSTOMER = 0;
  static readonly VENDOR = 1;
  static readonly OPERATOR = 2;

  private pisLoggedIn: boolean;
  private paccessLevel: number;
  private pusername: string;

  isLoggedInSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isUnAuthedSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn(): boolean { return this.pisLoggedIn; }
  get accessLevel(): number { return this.paccessLevel; }
  get username(): string { return this.pusername; }

  constructor() { this.updateInfo(); }

  updateInfo(callUnauth?: boolean) {
    this.pusername = this.getCookie('displayName') || '';
    const access = this.getCookie('accessLevel');
    if ((/[0-2]{1}/).test(access)) {
      this.paccessLevel = parseInt(access, 10);
    } else {
      this.paccessLevel = -1;
    }
    this.pisLoggedIn = this.paccessLevel >= 0;
    this.isLoggedInSub.next(this.pisLoggedIn);
    if (callUnauth && !this.pisLoggedIn) {
      this.isUnAuthedSub.next(true);
    }
  }

  emitLoggedIn() {
    this.isLoggedInSub.next(this.pisLoggedIn);
  }

  private getCookie(name: string) {
    const value = '; ' + document.cookie;
    const parts = value.split('; ' + name + '=');
    if (parts.length === 2) { return parts.pop().split(';').shift(); }
  }
}
