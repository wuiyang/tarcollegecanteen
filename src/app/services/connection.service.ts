import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthUserService } from './auth-user.service';
import { share } from 'rxjs/operators';
import { Url } from '../models/geturl.model';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private readonly header: HttpHeaders = new HttpHeaders();
  constructor(private http: HttpClient, private authService: AuthUserService) {
    this.header.set('Content-type', 'application/json');
  }

  get(url: string): Observable<any> {
    const subscribable: Observable<any> = this.http.get(url, { withCredentials: true }).pipe(share());
    if (url !== Url.creditInfoUrl) {
      this.afterReceive(subscribable);
    }
    return subscribable;
  }

  post(url: string, data: any, progressSubscribe?: boolean): Observable<any> {
    progressSubscribe = progressSubscribe || false;
    if (progressSubscribe) {
      return this.http.request(new HttpRequest('POST', url, data, { reportProgress: true, withCredentials: true }));
    }
    const subscribable: Observable<any> = this.http.post(url, data, {
      headers: this.header,
      withCredentials: true
    }).pipe(share());
    if (url.indexOf(Url.accountUrl) === -1) {
      this.afterReceive(subscribable);
    } else if (url === Url.changePassUrl) {
      subscribable.subscribe((receive) => { this.checkIsLogout(receive.message); });
    }
    return subscribable;
  }

  delete(url: string): Observable<any> {
    const subscribable: Observable<any> = this.http.delete(url, { withCredentials: true }).pipe(share());
    this.afterReceive(subscribable);
    return subscribable;
  }

  private afterReceive(subscribable: Observable<any>) {
    subscribable.subscribe(() => {
      this.authService.emitLoggedIn();
    }, (error) => {
      this.checkIsLogout(error.error.message);
    });
  }
  private checkIsLogout(message: string) {
    const callUnauth = message && (message.indexOf('session has expired') > 0 ||
    message.indexOf('not login') > 0 || message.indexOf('login again') > 0);
    this.authService.updateInfo(callUnauth);
  }
}
