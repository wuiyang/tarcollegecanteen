import { Injectable } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { Feedback } from 'src/app/models/feedback.model';
import { Url } from 'src/app/models/geturl.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FeedbackMessage } from 'src/app/models/feedback-message.model';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private connection: ConnectionService) { }

  createFeedback(feedback: Feedback, message: string): Observable<any> {
    const feedbackData = { orderId: feedback.orderId, message };
    return this.connection.post(Url.feedbackCreateUrl, feedbackData);
  }

  createFeedbackReply(feedback: Feedback, message: string): Observable<any> {
    const feedbackData = { id: feedback.id, message };
    return this.connection.post(Url.feedbackReplyUrl + feedback.id, feedbackData);
  }

  getFeedbackList(): Observable<any> {
    return this.connection.get(Url.feedbackRetriveUrl + 'all').pipe(
      map(d => {
        const list = d.data.map(feedbackData =>
          new Feedback(feedbackData.orderId ? ('Order ID: ' + feedbackData.orderId) : 'System', feedbackData.id,
                       feedbackData.orderId, [new FeedbackMessage(feedbackData.message, false)])
        );
        return list;
      })
    );
  }

  getFeedbackById(id: string): Observable<any> {
    return this.connection.get(Url.feedbackRetriveUrl + id).pipe(
      map(d => {
        const feedbackData = d.data;
        const feedback = new Feedback(feedbackData.orderId ? ('Order ' + feedbackData.orderId) : 'System',
                                      feedbackData.id, feedbackData.orderId);
        feedback.messages = feedbackData.messages.map(message =>
          new FeedbackMessage(message.message, message.isSelfSend, new Date(message.date))
        );
        return feedback;
      })
    );
  }

}
