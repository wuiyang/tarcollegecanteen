import { TestBed } from '@angular/core/testing';

import { CreditPointService } from './credit-point.service';

describe('CreditPointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreditPointService = TestBed.get(CreditPointService);
    expect(service).toBeTruthy();
  });
});
