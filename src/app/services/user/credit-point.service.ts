import { Injectable, EventEmitter } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { CreditPoint } from 'src/app/models/credit-point.model';
import { Url } from 'src/app/models/geturl.model';
import { CreditTransaction } from 'src/app/models/credit-transaction.model';
import { Observable, Subscription } from 'rxjs';
import { map, first } from 'rxjs/operators';
import { AuthUserService } from '../auth-user.service';

@Injectable({
  providedIn: 'root'
})
export class CreditPointService {
  private credit: CreditPoint;
  private userId: string;
  private error;
  private creditAmountEmitter: EventEmitter<number> = new EventEmitter();

  constructor(private connection: ConnectionService, private authService: AuthUserService) {
    this.retrieveCreditInfo();
    authService.isLoggedInSub.subscribe(() => {
      this.retrieveCreditInfo();
    });
  }

  get amount(): number {
    if (this.error) {
      throw this.error;
    }
    return (this.credit && this.credit.amount) || 0;
  }

  get user(): string {
    return this.userId;
  }

  get emitter(): EventEmitter<number> {
    return this.creditAmountEmitter;
  }

  sendCredit(toUser: string, amount: number, description: string, preorderId?: string): Observable<any> {
    return this.createTransaction(this.userId, toUser, amount, description, preorderId);
  }

  requestCredit(fromUser: string, amount: number, description: string): Observable<any> {
    return this.createTransaction(fromUser, this.userId, amount, description);
  }

  exchangeCredit(toUser: string, isTopup: boolean, amount: number): Observable<any> {
    const fromUserId = isTopup ? this.userId : toUser;
    const toUserId = isTopup ? toUser : this.userId;
    return this.createTransaction(fromUserId, toUserId, amount, this.getExchangeDescription(isTopup, amount), null, true);
  }

  private createTransaction(fromUser: string, toUser: string, amount: number, description: string,
                            preorderId?: string, topup?: boolean): Observable<any> {
    const trans: CreditTransaction = new CreditTransaction(null, fromUser, toUser, amount, description,
                                                           new Date(), 0, false, preorderId, topup || false);
    return this.uploadTransaction(trans);
  }

  retrieveCreditInfo() {
    this.connection.get(Url.creditInfoUrl).subscribe(data => {
      if (data.data) {
        this.error = null;
        this.credit = new CreditPoint(data.data.amount);
        this.userId = data.data.user;
      } else {
        this.credit = null;
        this.userId = null;
        this.error = {code: 300, message: 'Unable to retrieve credit points'};
      }
    }, error => {
      this.credit = null;
      this.userId = null;
      this.error = error;
    });
  }

  retrieveCreditTransactionList(month?: string): Observable<any> {
    return this.connection.get(Url.creditTransactionsListUrl + (month ? '/' + month : '')).pipe(
      map(data => {
        // occupy first index for start balance
        const transactionList: CreditTransaction[] = [null];
        if (data.code === 2) {
          data.data.forEach(transactionData => {
            const transaction = new CreditTransaction(transactionData.id, transactionData.fromUser, transactionData.toUser,
              transactionData.amount, transactionData.description, new Date(transactionData.date), transactionData.balance,
              transactionData.verified, transactionData.preorderId);
            transactionList.push(transaction);
          });

          if (transactionList.length === 1) {
            transactionList[0] = new CreditTransaction('-----END-----', null, null, 0, '--------No transaction made in this month--------',
                                                       null, 0, false);
            if (month) {
              transactionList[0].date = new Date(+month.substr(0, 4), +month.substr(4) + 1, 0);
            } else {
              transactionList[0].date = new Date();
            }
          } else {
            const firstTrans = transactionList[transactionList.length - 1];
            const firstBalance = firstTrans.balance + (firstTrans.amount * (firstTrans.fromUser === this.user ? 1 : -1));
            const lastTrans = transactionList[1];

            transactionList[0] = new CreditTransaction('-----END-----', null, null, 0, '----------Ending account balance----------',
                                                      new Date(lastTrans.date.getTime() + 1000), lastTrans.balance, false);

            transactionList.push(new CreditTransaction('-----START-----', null, null, 0, '---------Starting account balance----------',
                                                      new Date(firstTrans.date.getTime() - 1000), firstBalance, false));
          }
        }
        return transactionList;
      })
    );
  }

  retrieveCreditTopupList(): Observable<any> {
    return this.connection.get(Url.creditTopupListUrl).pipe(
      map(data => {
        const transactionList: CreditTransaction[] = [];
        if (data.code === 2) {
          data.data.forEach(transactionData => {
            const transaction = new CreditTransaction(transactionData.id, transactionData.fromUser, transactionData.toUser,
              transactionData.amount, transactionData.description, new Date(transactionData.date), transactionData.balance,
              transactionData.verified, transactionData.preorderId);
            transactionList.push(transaction);
          });
          if (transactionList.length === 0) {
            transactionList[0] = new CreditTransaction('-----END-----', null, null, 0, '----------No topup has made----------',
                                                      new Date(), 0, false);
          }
        }
        return transactionList;
      })
    );
  }

  confirmRequestTransaction(transaction: CreditTransaction): Observable<any> {
    return this.connection.post(Url.creditConfirmTransactionUrl + transaction.id, null).pipe(
      map(data => {
        return data;
      })
    );
  }

  private uploadTransaction(transaction: CreditTransaction): Observable<any> {
    return this.connection.post(Url.creditCreateTransactionUrl, transaction).pipe(
      map(data => {
        if (data.code === 1) {
          transaction.id = data.id;
          return transaction;
        } else {
          return data;
        }
      })
    );
  }

  private getExchangeDescription(isTopup: boolean, amount: number) {
    let message = 'Exchange from ' + (isTopup ? ('RM ' + (amount / 10)) : (amount + ' credit point'));
    message += ' to ' + (isTopup ? (amount + ' credit point') : ('RM ' + (amount / 10)));
    message += ' @ RM 0.1/CP';
    return message;
  }

}
