import { Injectable } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { Url } from 'src/app/models/geturl.model';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  constructor(private connection: ConnectionService) { }

  uploadPicture(picture: File): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getBase64(picture).then(base64 => {
        resolve(this.connection.post(Url.pictureUrl, { base64, name: picture.name }, true));
      }).catch(error => {
        reject(error);
      });
    });
  }

  private getBase64(file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
}
