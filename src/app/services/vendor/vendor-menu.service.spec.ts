import { TestBed } from '@angular/core/testing';

import { VendorMenuService } from './vendor-menu.service';

describe('VendorMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VendorMenuService = TestBed.get(VendorMenuService);
    expect(service).toBeTruthy();
  });
});
