import { TestBed } from '@angular/core/testing';

import { VendorOrderService } from './vendor-order.service';

describe('VendorOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VendorOrderService = TestBed.get(VendorOrderService);
    expect(service).toBeTruthy();
  });
});
