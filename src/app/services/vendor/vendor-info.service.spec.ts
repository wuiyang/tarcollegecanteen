import { TestBed } from '@angular/core/testing';

import { VendorInfoService } from './vendor-info.service';

describe('VendorInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VendorInfoService = TestBed.get(VendorInfoService);
    expect(service).toBeTruthy();
  });
});
