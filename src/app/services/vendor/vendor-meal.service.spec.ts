import { TestBed } from '@angular/core/testing';

import { VendorMealService } from './vendor-meal.service';

describe('VendorMealService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VendorMealService = TestBed.get(VendorMealService);
    expect(service).toBeTruthy();
  });
});
