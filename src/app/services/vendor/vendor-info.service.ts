import { Injectable } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { Observable } from 'rxjs';
import { Url } from 'src/app/models/geturl.model';
import { Vendor } from 'src/app/models/vendor.model';

@Injectable({
  providedIn: 'root'
})
export class VendorInfoService {

  constructor(private connection: ConnectionService) { }

  getVendorInfo(): Observable<any> {
    return this.getInfo();
  }

  getFullVendorInfo(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getVendorInfo().subscribe(data => {
        if (data.code === 1) {
          const hasInfo: boolean = data.data.name != null;
          const vendor: Vendor = new Vendor(data.data.id, data.data.name, data.data.description, data.data.pictureId,
                                            data.data.pictureUrl, !hasInfo, hasInfo);
          resolve(vendor);
        } else {
          reject(data);
        }
      }, error => {
        reject(error);
      });
    });
  }

  uploadVendorInfo(data: any): Observable<any> {
    return this.postInfo(data);
  }

  private getInfo(): Observable<any> {
    return this.connection.get(Url.vendorGetInfoUrl);
  }
  private postInfo(data: Vendor): Observable<any> {
    return this.connection.post(Url.vendorUpdateInfoUrl, data);
  }
}
