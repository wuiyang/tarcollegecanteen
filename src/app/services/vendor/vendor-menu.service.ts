import { Injectable } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { Observable } from 'rxjs';
import { Url } from 'src/app/models/geturl.model';
import { Meal } from 'src/app/models/meal.model';
import { map } from 'rxjs/operators';
import { Menu } from 'src/app/models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class VendorMenuService {

  constructor(private connection: ConnectionService) { }

  getAllMenuDate(): Observable<any> {
    return this.connection.get(Url.vendorMenuAllDateUrl).pipe(
      map(data => {
        return data.data.map(dateIsoString => new Date(dateIsoString));
      })
    );
  }

  getMenuInfo(date: Date, isLunch?: boolean): Observable<any> {
    return this.getMenu(date, isLunch).pipe(
      map(d => {
        if (d.code === 2) {
          const menuData = d.data;
          const mealList: Meal[] = [];
          const ret: Menu = new Menu(menuData.id, null, new Date(menuData.date), menuData.isLunch, mealList);
          menuData.meals.forEach((mealData) => {
            mealList.push(new Meal(mealData.id, mealData.name, mealData.description, mealData.price,
                                   mealData.pictureId, mealData.pictureUrl, null, mealData.calories));
          });
          return ret;
        } else {
          return new Menu(null, null, date, isLunch || false);
        }
      })
    );
  }

  uploadMenuInfo(menu: Menu): Observable<any> {
    return this.postMenu(menu.id ? (Url.vendorMenuUpdateUrl + menu.id) : Url.vendorMenuCreateUrl, menu);
  }

  deleteMenuInfo(menu: Menu): Observable<any> {
    return this.deleteMenu(menu);
  }

  private getMenu(date: Date, isLunch?: boolean): Observable<any> {
    return this.connection.get(Url.vendorMenuRetrieveUrl + date.toDateString() + '/' + (isLunch ? 'lunch' : 'breakfast'));
  }
  private postMenu(url: string, menu: Menu): Observable<any> {
    return this.connection.post(url, menu);
  }
  private deleteMenu(menu: Menu): Observable<any> {
    return this.connection.delete(Url.vendorMenuDeleteUrl + menu.id);
  }
}
