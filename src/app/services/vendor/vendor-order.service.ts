import { Injectable } from '@angular/core';
import { ConnectionService } from '../connection.service';
import { Observable } from 'rxjs';
import { Url } from 'src/app/models/geturl.model';
import { map } from 'rxjs/operators';
import { Menu } from 'src/app/models/menu.model';
import { Meal } from 'src/app/models/meal.model';
import { Ingredient } from 'src/app/models/ingredient.model';
import { Order } from 'src/app/models/order.model';
import { OrderDetail } from 'src/app/models/order-detail.model';

@Injectable({
  providedIn: 'root'
})
export class VendorOrderService {

  constructor(private connection: ConnectionService) { }

  getMenuWithOrderList(): Observable<any> {
    return this.connection.get(Url.vendorOrderMenuListUrl + 'all').pipe(
      map(data => data.data.map(menuData => new Menu(menuData.id, null, new Date(menuData.date), menuData.isLunch)))
    );
  }

  getOrderListByMenuId(menu: Menu): Observable<any> {
    return this.connection.get(Url.vendorOrderMenuListUrl + menu.id).pipe(
      map(data => {
        const mealHashMap = {};
        const menuData = data.data.menu;
        const orderDataList = data.data.order;
        menu.meals = [];
        Object.keys(menuData.meals).forEach(mealId => {
          const mealData = menuData.meals[mealId];
          const meal = new Meal(mealData.id, mealData.name, mealData.description, mealData.price, null, mealData.pictureUrl,
                                null, mealData.calories);
          meal.ingredients = mealData.ingredients.map(ing => new Ingredient(ing.name, ing.quantity, ing.unit));
          mealHashMap[mealData.id] = meal;
          menu.meals.push(meal);
        });
        const orderList = orderDataList.map(orderData => {
          const order = new Order(orderData.id, menu, orderData.createDate, orderData.lastModifyDate, orderData.cancel);
          order.details = orderData.details.map(detail => new OrderDetail(mealHashMap[detail.mealId], detail.quantity));
          return order;
        });
        return orderList;
      })
    );
  }

  verifyOrder(orderJson: any): Observable<any> {
    return this.connection.post(Url.vendorOrderVerifyUrl, orderJson).pipe(
      map(data => {
        if (data.code !== 1) {
          return data;
        }
        const mealHashMap = {};
        const orderData = data.data.order;
        const difference = data.data.difference;
        const menu = new Menu(orderData.menu.id, null, new Date(orderData.menu.date), orderData.menu.isLunch);
        menu.meals = orderData.menu.meals.map(mealData => {
          const meal = new Meal(mealData.id, mealData.name, mealData.description, mealData.price,
                                null, mealData.pictureUrl, null, mealData.calories);
          mealHashMap[mealData.id] = meal;
          return meal;
        });
        const order = new Order(orderData.id, menu, new Date(orderData.createDate), new Date(orderData.lastModifyDate), false,
                                orderData.receiveTime, orderData.completeTime, orderData.customer);
        order.details = orderData.details.map(detail =>
          new OrderDetail(mealHashMap[detail.mealId] || new Meal(detail.mealId, '?????'), detail.quantity)
        );
        return {order, difference, message: data.message};
      })
    );
  }

  approvePreorder(orderJson: any): Observable<any> {
    return this.connection.post(Url.vendorOrderApproveUrl, orderJson);
  }
}
