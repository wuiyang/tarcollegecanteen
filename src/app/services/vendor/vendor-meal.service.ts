import { Injectable } from '@angular/core';
import { Meal } from 'src/app/models/meal.model';
import { Url } from 'src/app/models/geturl.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConnectionService } from '../connection.service';
import { Ingredient } from 'src/app/models/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class VendorMealService {

  constructor(private connection: ConnectionService) { }

  getMealList(): Observable<any> {
    return this.getMeal().pipe(
      map(d => {
        const ret: Meal[] = [];
        d.data.forEach((mealData) => {
          const meal: Meal = new Meal(mealData.id, mealData.name, mealData.description, mealData.price,
                            mealData.pictureId, mealData.pictureUrl, null, mealData.calories);
          if (mealData.ingredients) {
            meal.ingredients = mealData.ingredients.map(ing => new Ingredient(ing.name, ing.quantity, ing.unit));
          }
          ret.push(meal);
        });
        return ret;
      })
    );
  }

  uploadMealInfo(meal: Meal): Observable<any> {
    const url = meal.id ? (Url.vendorMealUpdateUrl + meal.id) : Url.vendorMealCreateUrl;
    return this.postMeal(url, meal);
  }

  deleteMealInfo(meal: Meal): Observable<any> {
    return this.deleteMeal(meal);
  }

  private getMeal(): Observable<any> {
    return this.connection.get(Url.vendorMealRetriveUrl);
  }
  private postMeal(url: string, meal: Meal): Observable<any> {
    return this.connection.post(url, meal);
  }
  private deleteMeal(meal: Meal): Observable<any> {
    return this.connection.delete(Url.vendorMealDeleteUrl + meal.id);
  }
}
