import { Injectable } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { Menu } from 'src/app/models/menu.model';
import { Meal } from 'src/app/models/meal.model';
import { Observable } from 'rxjs';
import { ConnectionService } from '../connection.service';
import { Url } from 'src/app/models/geturl.model';
import { map } from 'rxjs/operators';
import { Vendor } from 'src/app/models/vendor.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  preorderList: Order[] = [];

  constructor(private connection: ConnectionService) { }

  getOrder(orderIndex: number): Order {
    return this.preorderList[orderIndex];
  }

  getOrderDetail(menu: Menu, meal: Meal): OrderDetail {
    const orderIndex = this.preorderList.findIndex(
      o => o.menu.date.getTime() === menu.date.getTime() && o.menu.isLunch === menu.isLunch && o.menu.vendor.id === menu.vendor.id
    );
    if (orderIndex === -1) {
      return null;
    }
    const order = this.preorderList[orderIndex];
    const detailIndex = order.details.findIndex(detail => detail.meal.id === meal.id);
    return detailIndex === -1 ? null : order.details[detailIndex];
  }

  get orderCount(): number {
    return this.preorderList.length;
  }

  updateOrder(menu: Menu, orderDetail: OrderDetail) {
    let order = this.preorderList.find(
      o => o.menu.date.getTime() === menu.date.getTime() && o.menu.isLunch === menu.isLunch && o.menu.vendor.id === menu.vendor.id
    );
    let detailIndex = -1;
    if (order == null) {
      order = new Order(null, menu);
      this.preorderList.push(order);
    } else {
      detailIndex = order.details.findIndex(detail => detail.meal.id === orderDetail.meal.id);
    }

    if (detailIndex === -1) {
      order.details.push(orderDetail);
    } else {
      order.details[detailIndex].quantity = orderDetail.quantity;
    }
  }

  deleteOrder(menu: Menu, orderDetail: OrderDetail) {
    const orderIndex = this.preorderList.findIndex(
      o => o.menu.date.getTime() === menu.date.getTime() && o.menu.isLunch === menu.isLunch && o.menu.vendor.id === menu.vendor.id
    );
    if (orderIndex === -1) {
      return;
    }
    const order = this.preorderList[orderIndex];
    if (order.details.length <= 1) {
      this.preorderList.splice(orderIndex, 1);
      return;
    }
    const mealIndex = order.details.findIndex(detail => detail.meal.id === orderDetail.meal.id);
    if (mealIndex === -1) {
      return;
    }
    order.details.splice(mealIndex, 1);
  }

  deleteOrderByIndex(order: Order, orderDetailIndex: number) {
    const menuIndex = this.preorderList.findIndex(o =>
      o.menu.date.getTime() === order.menu.date.getTime() &&
      o.menu.isLunch === order.menu.isLunch &&
      o.menu.vendor.id === order.menu.vendor.id
    );

    if (menuIndex < 0) {
      return;
    }

    if (this.preorderList[menuIndex].details.length > 1) {
      this.preorderList[menuIndex].details.splice(orderDetailIndex, 1);
    } else {
      this.preorderList.splice(menuIndex, 1);
    }
  }

  deleteAllOrder(order: Order) {
    const index = this.preorderList.findIndex(preorder =>
      preorder.menu.vendor.id === order.menu.vendor.id &&
      preorder.menu.date.getTime() === order.menu.date.getTime() &&
      preorder.menu.isLunch === order.menu.isLunch
    );
    if (index < 0) {
      return;
    }
    this.preorderList.splice(index, 1);
  }

  placeOrder(order: Order): Observable<any> {
    const orderData = { menuId: order.menu.id, date: order.menu.date, details: order.details.map(
      orderDetail => ({ mealId: orderDetail.meal.id, quantity: orderDetail.quantity })
    )};
    return this.connection.post(Url.orderCreateUrl, orderData);
  }

  getOrderList(getPast: boolean, getFuture: boolean): Observable<any> {
    let url;
    if (getPast && getFuture) {
      url = Url.orderRetriveAllUrl;
    } else if (getPast) {
      url = Url.orderHistoryUrl;
    } else if (getFuture) {
      url = Url.orderRetriveFutureUrl;
    }
    return this.connection.get(url).pipe(
      map(data => {
        const orderList = [];
        data.data.forEach(orderData => {
          const order = new Order(orderData.id, new Menu(
                                    orderData.menu.id,
                                    new Vendor(null, orderData.menu.vendor.name, orderData.menu.vendor.description),
                                    new Date(orderData.menu.date), orderData.menu.isLunch
                                  ), new Date(orderData.createDate), new Date(orderData.lastModifyDate), orderData.cancel);

          order.details = orderData.details.map(details => new OrderDetail(new Meal(details.meal.id, details.meal.name,
            details.meal.description, details.meal.price, null, details.meal.pictureUrl, null, details.meal.calories), details.quantity)
          );
          orderList.push(order);
        });
        return orderList;
      })
    );
  }

  modifyOrder(order: Order): Observable<any> {
    const orderData = { details: order.details.map(
      orderDetail => ({ mealId: orderDetail.meal.id, quantity: orderDetail.quantity })
    )};
    return this.connection.post(Url.orderModifyUrl + order.id, orderData);
  }

  cancelOrder(order: Order): Observable<any> {
    return this.connection.post(Url.orderCancelUrl + order.id, null);
  }

  getConsumptionData(date: Date, type: string): Observable<any> {
    return this.connection.get(Url.orderConsumptionUrl + date.toISOString() + '/' + type).pipe(
      map(d => {
        let total = 0;
        let count = 0;
        let dayCount = 0;
        const dateConsumptionHashMap = d.data;
        const dateConsumptionArray = Object.keys(dateConsumptionHashMap).map(str => {
          const dateGet: Date = new Date(str.substr(0, str.length - 2));
          const isLunch: boolean = str.substr(str.length - 1) === '1';
          const consumption: number = dateConsumptionHashMap[str];
          total += consumption;
          count++;
          return { date: dateGet, isLunch, consumption };
        }).sort((a: {date: Date, isLunch: boolean, consumption: number}, b: {date: Date, isLunch: boolean, consumption: number}) => {
          const diff = a.date.getTime() - b.date.getTime();
          if (diff === 0) { dayCount--; return a.isLunch ? 1 : 0; }
          return diff;
        });
        dayCount += count;
        return { dateConsumptionArray, total, count, dayCount };
      })
    );
  }
}
