import { Injectable } from '@angular/core';
import { Url } from '../models/geturl.model';
import { Observable } from 'rxjs';
import { ConnectionService } from './connection.service';

@Injectable()
export class LoginService {
  constructor(private connection: ConnectionService) { }

  login(data: any): Observable<any> {
    return this.postCredentials(Url.loginUrl, data);
  }

  register(data: any): Observable<any> {
    return this.postCredentials(Url.registerUrl, data);
  }

  reset(data: any): Observable<any> {
    return this.postCredentials(Url.resetUrl, data);
  }

  getInfo(): Observable<any> {
    return this.connection.get(Url.getInfoUrl);
  }

  changeInfo(data: any): Observable<any> {
    return this.postCredentials(Url.changeInfoUrl, data);
  }

  changePassword(data: any): Observable<any> {
    return this.postCredentials(Url.changePassUrl, data);
  }

  logout() {
    return this.connection.get(Url.logoutUrl);
  }

  private postCredentials(url: string, data?: any): Observable<any> {
    return this.connection.post(url, data);
  }
}
