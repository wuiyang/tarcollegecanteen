import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ScrollToBottomService } from './services/page/scroll-to-bottom.service';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  @ViewChild('mainContainer') container: ElementRef;
  hasHide = false;

  constructor(private scrollService: ScrollToBottomService, private titleService: Title, private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.scrollService.scrollEmitter.subscribe(scroll => {
      if (scroll) {
        this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight;
      }
    });
    this.scrollService.heightEmitter.subscribe(height => {
      this.hasHide = height;
    });
    this.router.events.subscribe((event: NavigationEnd | any) => {
      if (event instanceof NavigationEnd) {
        this.applyTitle();
      }
    });
  }

  applyTitle() {
    let route = this.activatedRoute;
    while (route.firstChild) {
      route = route.firstChild;
    }
    if (route.outlet === 'primary') {
      route.data.subscribe((e) => {
        this.titleService.setTitle(e.title + ' - TARUC Canteen');
      });
    }
  }
}
