export class CreditPoint {
  private _amount: number;
  constructor(amount: number) {
    this._amount = amount;
  }
  get amount(): number {
    return this._amount;
  }

  deduct(amount: number) {
    this._amount -= amount;
  }
}
