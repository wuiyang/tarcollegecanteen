export class CreditTransaction {
  id: string;
  fromUser: string;
  toUser: string;
  amount: number;
  description: string;
  date: Date;
  verified: boolean;
  balance: number;
  preorderId: string;
  isTopup: boolean;

  constructor(id: string, fromUser: string, toUser: string, amount: number, description: string, date: Date,
              balance: number, verified: boolean, preorderId?: string, isTopup?: boolean) {
    this.id = id || '';
    this.fromUser = fromUser || '';
    this.toUser = toUser || '';
    this.amount = amount || 0;
    this.description = description || '';
    this.date = date;
    this.balance = balance;
    this.verified = verified;
    this.preorderId = preorderId;
    this.isTopup = isTopup || false;
  }
}
