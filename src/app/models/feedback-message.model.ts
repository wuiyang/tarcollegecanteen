export class FeedbackMessage {
  message: string;
  isSelfSend: boolean;
  date: Date;
  constructor(message: string, isSelfSend: boolean, date?: Date) {
    this.message = message;
    this.isSelfSend = isSelfSend;
    this.date = date;
  }
}
