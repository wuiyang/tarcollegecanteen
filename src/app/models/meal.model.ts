import { Ingredient } from './ingredient.model';

export class Meal {
  id: string;
  name: string;
  description: string;
  price: number;
  pictureId: string;
  pictureUrl: string;
  ingredients: Ingredient[];
  calories: number;
  editable: boolean;

  constructor(id: string, name?: string, description?: string, price?: number, pictureId?: string,
              pictureUrl?: string, ingredients?: Ingredient[], calories?: number, editable?: boolean) {
    this.id = id || '';
    this.name = name || '';
    this.description = description || '';
    this.price = price || 0;
    this.pictureId = pictureId || '';
    this.pictureUrl = pictureUrl || '';
    this.ingredients = ingredients || [];
    this.calories = calories || 0;
    this.editable = editable || false;
  }
}
