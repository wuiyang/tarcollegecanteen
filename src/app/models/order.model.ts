import { OrderDetail } from './order-detail.model';
import { Menu } from './menu.model';

export class Order {
  id: string;
  menu: Menu;
  createDate: Date;
  lastModifyDate: Date;
  cancel: boolean;
  receiveTime: Date;
  completeTime: Date;
  customer: string;
  details: OrderDetail[];


  constructor(id: string, menu: Menu, createDate?: Date, lastModifyDate?: Date, cancel?: boolean, receiveTimestr?: string,
              completeTimestr?: string, customer?: string, details?: OrderDetail[]) {
    this.id = id || '';
    this.menu = menu || new Menu();
    this.createDate = createDate;
    this.lastModifyDate = lastModifyDate;
    this.cancel = cancel || false;
    this.receiveTime = receiveTimestr ? new Date(receiveTimestr) : null;
    this.completeTime = completeTimestr ? new Date(completeTimestr) : null;
    this.customer = customer;
    this.details = details || [];
  }

  getTotalPrice(): number {
    let total = 0;
    this.details.forEach(orderDetail => {
      total += orderDetail.meal.price * orderDetail.quantity;
    });
    return total;
  }
}
