import { Meal } from './meal.model';

export class OrderDetail {
  meal: Meal;
  quantity: number;

  constructor(meal: Meal, quantity: number) {
    this.meal = meal;
    this.quantity = quantity;
  }
}
