import { Meal } from './meal.model';
import { Vendor } from './vendor.model';

export class Menu {
  private static readonly DEFAULT_DATE_START = new Date('1970-01-03');
  private static readonly DEFAULT_DATE_END = new Date('1970-01-12');
  id: string;
  vendor: Vendor;
  private _date: Date;
  isLunch: boolean;
  meals: Meal[];
  editable: boolean;
  isDefault: boolean;

  constructor(id?: string, vendor?: Vendor, date?: Date, isLunch?: boolean, meals?: Meal[]) {
    this.id = id || '';
    this.vendor = vendor || new Vendor();
    this.date = date || Menu.getMinimumEditableDate();
    this.isLunch = isLunch || false;
    this.meals = meals || [];
  }

  get date() {
    return this._date;
  }

  set date(date: Date) {
    this._date = date;
    this.isDefault = this.date >= Menu.DEFAULT_DATE_START && this.date <= Menu.DEFAULT_DATE_END;
    this.editable = this.date >= Menu.getMinimumEditableDate() || this.isDefault;
  }

  static getMinimumEditableDate(): Date {
    const date: Date = new Date();
    date.setDate(date.getDate() + 2);
    return new Date(date.toDateString() + ' GMT+0');
  }

  static getMinimumCancelableDate(): Date {
    const date: Date = new Date();
    date.setDate(date.getDate() + 1);
    return new Date(date.toDateString() + ' GMT+0');
  }
}
