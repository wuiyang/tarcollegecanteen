import { HttpErrorResponse } from '@angular/common/http';

export class Url {
  private static readonly mainUrl = 'http://' + window.location.host.slice(0, -5) + ':8963/api/';

  // private static readonly mainUrl = window.location.origin + '/api/';

  // picture API
  static readonly pictureUrl = Url.mainUrl + 'picture';

  // login and register API
  static readonly accountUrl = Url.mainUrl + 'account/';
  static readonly loginUrl = Url.accountUrl + 'login';
  static readonly registerUrl = Url.accountUrl + 'register';
  static readonly resetUrl = Url.accountUrl + 'reset';
  static readonly getInfoUrl = Url.accountUrl + 'info';
  static readonly changeInfoUrl = Url.accountUrl + 'updateinfo';
  static readonly changePassUrl = Url.accountUrl + 'changepass';
  static readonly logoutUrl = Url.accountUrl + 'logout';

  // Menu API
  static readonly menuApiUrl = Url.mainUrl + 'menu/';
  static readonly menuRetriveUrl = Url.menuApiUrl + 'get';

  // Feedback API
  static readonly feedbackApiUrl = Url.mainUrl + 'feedback/';
  static readonly feedbackCreateUrl = Url.feedbackApiUrl + 'new';
  static readonly feedbackRetriveUrl = Url.feedbackApiUrl + 'get/';
  static readonly feedbackReplyUrl = Url.feedbackApiUrl + 'reply/';

  // Order API
  static readonly orderApiUrl = Url.mainUrl + 'order/';
  static readonly orderHistoryUrl = Url.orderApiUrl + 'history';
  static readonly orderRetriveAllUrl = Url.orderApiUrl + 'getall';
  static readonly orderRetriveFutureUrl = Url.orderApiUrl + 'getfuture';
  static readonly orderConsumptionUrl = Url.orderApiUrl + 'consumption/';
  static readonly orderRetriveUrl = Url.orderApiUrl + 'get/';
  static readonly orderCreateUrl = Url.orderApiUrl + 'new';
  static readonly orderModifyUrl = Url.orderApiUrl + 'modify/';
  static readonly orderCancelUrl = Url.orderApiUrl + 'cancel/';

  // Credit API
  static readonly creditApiUrl = Url.mainUrl + 'credit/';
  static readonly creditInfoUrl = Url.creditApiUrl + 'info';
  static readonly creditTopupListUrl = Url.creditApiUrl + 'topuplist';
  static readonly creditTransactionsListUrl = Url.creditApiUrl + 'transactions';
  static readonly creditCreateTransactionUrl = Url.creditApiUrl + 'transaction/create';
  static readonly creditConfirmTransactionUrl = Url.creditApiUrl + 'transaction/confirm/';

  // vendor API
  static readonly vendorApiUrl = Url.mainUrl + 'vendor/';

  // vendor meal management API
  static readonly vendorMealUrl = Url.vendorApiUrl + 'meal/';
  static readonly vendorMealCreateUrl = Url.vendorMealUrl + 'new';
  static readonly vendorMealRetriveUrl = Url.vendorMealUrl + 'all';
  static readonly vendorMealUpdateUrl = Url.vendorMealUrl + 'modify/';
  static readonly vendorMealDeleteUrl = Url.vendorMealUrl + 'delete/';

  // vendor menu management API
  static readonly vendorMenuUrl = Url.vendorApiUrl + 'menu/';
  static readonly vendorMenuCreateUrl = Url.vendorMenuUrl + 'new';
  static readonly vendorMenuRetrieveUrl = Url.vendorMenuUrl + 'get/';
  static readonly vendorMenuAllDateUrl = Url.vendorMenuUrl + 'alldate';
  static readonly vendorMenuUpdateUrl = Url.vendorMenuUrl + '/modify/';
  static readonly vendorMenuDeleteUrl = Url.vendorMenuUrl + 'delete/';

  // vendor order management API
  static readonly vendorOrderUrl = Url.vendorApiUrl + 'order/';
  static readonly vendorOrderMenuListUrl = Url.vendorOrderUrl + 'list/menu/';
  static readonly vendorOrderVerifyUrl = Url.vendorOrderUrl + 'verify';
  static readonly vendorOrderApproveUrl = Url.vendorOrderUrl + 'approve';

  // vendor info API
  static readonly vendorGetInfoUrl = Url.vendorApiUrl + 'info';
  static readonly vendorUpdateInfoUrl = Url.vendorApiUrl + 'updateinfo';

  static readonly pingUrl = Url.mainUrl + 'ping';

  static getErrorMessage(error: HttpErrorResponse) {
    return error.error.message ? error.error : error;
  }
}
