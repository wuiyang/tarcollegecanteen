export class UserInfo {
  id: string;
  displayName: string;
  email: string;
  idNumber: string;

  constructor(id?: string, displayName?: string, email?: string, idNumber?: string) {
    this.id = id || '';
    this.displayName = displayName || '';
    this.email = email || '';
    this.idNumber = idNumber || '';
  }
}
