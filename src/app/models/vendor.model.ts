export class Vendor {
  id: string;
  name: string;
  description: string;
  pictureId: string;
  pictureUrl: string;
  editable: boolean;
  hasInfo: boolean;

  constructor(id?: string, name?: string, description?: string, pictureId?: string,
              pictureUrl?: string, editable?: boolean, hasInfo?: boolean) {
    this.id = id || '';
    this.name = name || '';
    this.description = description || '';
    this.pictureId = pictureId || '';
    this.pictureUrl = pictureUrl || '';
    this.editable = editable || false;
    this.hasInfo = hasInfo || false;
  }
}
