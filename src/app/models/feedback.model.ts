import { FeedbackMessage } from './feedback-message.model';

export class Feedback {
  toUsername: string;
  id: string;
  messages: FeedbackMessage[];
  orderId: string;

  constructor(toUsername: string, id?: string, orderId?: string, messages?: FeedbackMessage[]) {
    this.toUsername = toUsername;
    this.id = id || '';
    this.orderId = orderId || '';
    this.messages = messages || [];
  }
}
