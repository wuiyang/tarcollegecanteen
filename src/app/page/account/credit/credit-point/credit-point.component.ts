import { Component, OnInit, ViewChild } from '@angular/core';
import { CreditTransaction } from 'src/app/models/credit-transaction.model';
import { CreditPointService } from 'src/app/services/user/credit-point.service';
import { MatSnackBar, MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { NewTransactionComponent } from 'src/app/page/dialog/new-transaction/new-transaction.component';
import { ConfirmComponent } from 'src/app/page/dialog/confirm/confirm.component';
import { MonthSelection } from 'src/app/models/month-selection.model';
import { FormControl } from '@angular/forms';
import { AuthUserService } from 'src/app/services/auth-user.service';

@Component({
  selector: 'app-credit-point',
  templateUrl: './credit-point.component.html',
  styleUrls: ['./credit-point.component.css']
})
export class CreditPointComponent implements OnInit {
  @ViewChild(MatSort) sortTable: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  loaded = false;
  transaction: CreditTransaction[];
  displayedColumns: string[] = ['transDate', 'transId', 'toUser', 'description', 'orderId', 'credit', 'debit', 'verification', 'amount'];
  transactionDataSource = new MatTableDataSource(this.transaction);
  monthSelection: MonthSelection[] = [new MonthSelection('First 200 latest transactions', '')];
  selectFormControl: FormControl = new FormControl('');

  constructor(public dialog: MatDialog, public creditService: CreditPointService,
              private auth: AuthUserService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.retrieveTransaction();
    this.transactionDataSource.sort = this.sortTable;
    this.transactionDataSource.paginator = this.paginator;
    this.transactionDataSource.sortingDataAccessor = (data, sortHeaderId) => {
      switch (sortHeaderId) {
        case 'transDate': return data.date.getTime();
        case 'transId': return data.id.toLowerCase();
        case 'toUser': return (data.fromUser === this.creditService.user ? data.toUser : data.fromUser).toLowerCase();
        case 'description': return data.description.toLowerCase();
        case 'orderId': return data.preorderId;
        case 'credit': return (data.fromUser === this.creditService.user ? 0 : data.amount);
        case 'debit': return (data.fromUser === this.creditService.user ? data.amount : 0);
        case 'verification': return data.verified ? 1 : (data.fromUser === this.creditService.user ? 2 : 0);
        default: return data.id;
      }
    };

    if (this.auth.accessLevel === AuthUserService.OPERATOR && this.displayedColumns.length > 8) {
      this.displayedColumns.pop();
    }

    // populate month selection
    const today = new Date();
    for (let i = 0; i < 6; i++) {
      const monthName = today.toLocaleString('en-us', { month: 'long' });
      const monthStr = ('0' + today.getMonth()).slice(-2);
      this.monthSelection.push(new MonthSelection(today.getFullYear() + ' ' + monthName, today.getFullYear() + monthStr));
      today.setMonth(today.getMonth() - 1);
    }
  }

  createNewTransaction() {
    this.dialog.open(NewTransactionComponent, {width: '350px'}).afterClosed().subscribe(updated => {
      if (updated) {
        this.retrieveTransaction();
      }
    });
  }

  retrieveTransaction(month?: string) {
    this.loaded = false;
    this.creditService.retrieveCreditTransactionList(month).subscribe(data => {
      this.transaction = data;
      this.transactionDataSource.data = this.transaction;
      this.loaded = true;
    }, error => {
      this.showSnackMessage(error.message);
    });
  }

  askConfirmTransaction(transaction: CreditTransaction) {
    this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: {
        header: 'Approve transaction',
        body: 'Do you confirm the following transaction?',
        detail: true,
        userId: transaction.toUser,
        amount: transaction.amount,
        description: transaction.description,
        cancel: 'No',
        action: 'Yes'
      }
    }).afterClosed().subscribe(accept => {
      if (accept) {
        this.creditService.confirmRequestTransaction(transaction).subscribe((data) => {
          this.retrieveTransaction();
          this.showSnackMessage(data.message);
        }, error => {
          this.retrieveTransaction();
          this.showSnackMessage(error.error.message || error.message);
        });
      }
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
