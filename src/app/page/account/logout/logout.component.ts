import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthUserService } from 'src/app/services/auth-user.service';

@Component({
  selector: 'app-logout',
  template: `<div><h2>Logging out...</h2></div>`,
  styles: [`div {
    align-items: center;
    display: flex;
    justify-content: center;
    height: 100%;
  }`]
})
export class LogoutComponent implements OnInit {

  constructor(private login: LoginService, private router: Router, private snackBar: MatSnackBar, private auth: AuthUserService) { }

  ngOnInit() {
    this.login.logout().subscribe(obj => {
      this.snackBar.open('You have logged out!', 'Dismiss', { duration: 2000 });
      this.auth.updateInfo();
      this.router.navigateByUrl('/');
    }, error => {
      let message = 'Unable to logout, are you connected to the internet?';
      if (error.error.message) {
        message = error.error.message;
      }
      this.snackBar.open(message, 'Dismiss', { duration: 2000 });
      this.auth.updateInfo();
      this.router.navigateByUrl('/');
    });
  }

}
