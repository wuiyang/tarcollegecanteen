import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { AuthUserService } from 'src/app/services/auth-user.service';
import { UserInfo } from 'src/app/models/user-info.model';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.css']
})
export class AccountInfoComponent implements OnInit {
  pageTab = 0;
  userInfo: UserInfo = new UserInfo();
  serverErrorMessage = {oldPassword: '', newPassword: ''};
  oldPassFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{1,}$')
  ]);
  newPassFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{1,}$')
  ]);

  formData: FormGroup = new FormGroup({
    oldPassword: this.oldPassFormControl,
    newPassword: this.newPassFormControl
  });

  constructor(private loginService: LoginService, public userAuth: AuthUserService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.loginService.getInfo().subscribe(data => {
      const userData = data.data;
      this.userInfo = new UserInfo(userData.id, userData.displayName, userData.email, userData.idNumber);
    }, error => {
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  modifyPassword() {
    this.loginService.changePassword(this.formData.value).subscribe(data => {
      this.showSnackMessage(data.message);
    }, error => {
      if (error.error.code) {
        if (error.error.code === 201) {
          this.serverErrorMessage.oldPassword = error.error.message;
          this.oldPassFormControl.setErrors({ serverInvalid: true });
          this.oldPassFormControl.markAsTouched();
        } else if (error.error.code === 300) {
          this.serverErrorMessage.newPassword = error.error.message;
          this.newPassFormControl.setErrors({ serverInvalid: true });
          this.newPassFormControl.markAsTouched();
        }
      } else {
        this.showSnackMessage(error.error.message || error.message);
      }
    });
  }

  checksamepass() {
    if (this.oldPassFormControl.value === this.newPassFormControl.value) {
      this.serverErrorMessage.newPassword = 'New password is same as old password!';
      this.newPassFormControl.setErrors({ serverInvalid: true });
      this.newPassFormControl.markAsTouched();
    }
  }

  changePageTab(tabNum: number) {
    this.pageTab = tabNum;
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
