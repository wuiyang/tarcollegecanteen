import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Url } from 'src/app/models/geturl.model';
import { MatSnackBar } from '@angular/material';
import { AuthUserService } from 'src/app/services/auth-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  type: number; // 0 = login, 1 = register, 2 = forget
  formData: FormGroup;
  serverErrorMessage: any = {username: '', password: '', email: '', collegeId: '', icNumber: '', vendorId: ''};
  registerType = 0; // 0 = customer, 1 = vendor, 2 = canteen operator
  forgotResponse = '';

  usernameFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(4),
    Validators.pattern('^[a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\+\\|\\{\\}\\:' +
                       '\\"\\<\\>\\?\\`\\-\\=\\\\\\[\\]\\;\\\'\\,\\.\\/]{1,}$')
  ]);

  passwordFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{1,}$')
  ]);

  emailFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z0-9.\\-_]{1,}@[a-zA-Z0-9.\\-]{2,}[.]{1}[a-zA-Z]{2,}$')
  ]);

  collegeIdFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^(?:(P|p|)[0-9]{4}|[0-9]{7})$') // (P)1234 or 1234567
  ]);

  // only check first 4 number, date have to check manually
  icNumberFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^([0-9]{2}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))[0-9]{6})$')
  ]);

  vendorIdFormControl: FormControl = new FormControl('', [
    Validators.pattern('^([0-9a-zA-Z]{10}){0,1}$')
  ]);

  rememberFormControl: FormControl = new FormControl();

  registerFormGroupList: Array<FormGroup>;


  constructor(private loginService: LoginService, private router: Router, private route: ActivatedRoute,
              private snackBar: MatSnackBar, private auth: AuthUserService) { }

  ngOnInit() {
    this.type = this.route.snapshot.data.type;
    if (this.type === 0) {
      this.formData = new FormGroup({
        username: this.usernameFormControl,
        password: this.passwordFormControl,
        rememberMe: this.rememberFormControl
      });
    } else if (this.type === 1) {
      this.formData = new FormGroup({
        username: this.usernameFormControl,
        password: this.passwordFormControl,
        email: this.emailFormControl,
        collegeId: this.collegeIdFormControl,
        rememberMe: this.rememberFormControl
      });
    } else {
      this.formData = new FormGroup({
        email: this.emailFormControl
      });
    }
  }

  keyEnterOnPassword() {
    this.type === 0 ? this.doLogin() : this.doRegister();
  }

  doLogin() {
    if (!this.formData.valid) {
      return;
    }
    this.loginService.login(this.formData.value).subscribe(obj => {
      this.showSnackMessage('You have successfully logged in!');
      this.auth.updateInfo();
      this.router.navigateByUrl('');
    },
    error => {
      this.errorToDisplay(Url.getErrorMessage(error));
    });
  }

  doRegister() {
    if (!this.formData.valid) {
      return;
    }
    this.loginService.register(this.formData.value).subscribe(obj => {
      this.showSnackMessage('You have successfully registered!');
      this.auth.updateInfo();
      this.router.navigateByUrl('');
    },
    error => {
      this.errorToDisplay(Url.getErrorMessage(error));
    });
  }

  sendForgot() {
    if (!this.formData.valid) {
      return;
    }
    this.loginService.reset(this.formData.value).subscribe(data => {
      this.showSnackMessage(data.message);
    }, error => {
      this.serverErrorMessage.email = error.error.message || error.message;
      this.emailFormControl.setErrors({ serverInvalid: true });
      this.emailFormControl.markAsTouched();
    });
  }

  updateRegister(type: string) {
    if (type === 'customer') {
      this.registerType = 0;
      if (this.formData.contains('icNumber')) {
        this.formData.removeControl('icNumber');
        this.formData.removeControl('vendorId');
        this.formData.addControl('collegeId', this.collegeIdFormControl);
      }
    } else if (type === 'vendor') {
      this.registerType = 1;
      if (this.formData.contains('collegeId')) {
        this.formData.removeControl('collegeId');
        this.formData.addControl('icNumber', this.icNumberFormControl);
        this.formData.addControl('vendorId', this.vendorIdFormControl);
      }
    }
  }

  errorToDisplay(error: any) {
    if (error.code === 300) { // custom error message
      error.message.forEach((element: { type: string; message: string; }) => {
        if (element.type === 'popup') {
          this.showSnackMessage(element.message);
        } else {
          this.serverErrorMessage[element.type] = element.message;
          this.formData.get(element.type).setErrors({ serverInvalid: true });
          this.formData.get(element.type).markAsTouched();
        }
      });
    } else {
      let choosenField: string;
      if (error.code === 200 || error.code === 202 || error.code === 301) {
        choosenField = 'username';
      } else if (error.code === 101 || error.code === 201) {
        choosenField = 'password';
      } else if (error.code === 125 || error.code === 203 || error.code === 204 || error.code === 205) {
        choosenField = 'email';
      } else if (error.code === 210) {
        choosenField = this.registerType === 0 ? 'collegeId' : 'icNumber';
      } else if (error.code === 399) {
        choosenField = 'vendorId';
      } else {
        this.showSnackMessage(error.message);
        return;
      }
      this.serverErrorMessage[choosenField] = error.message;
      this.formData.get(choosenField).setErrors({ serverInvalid: true });
      this.formData.get(choosenField).markAsTouched();
    }
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
