import { Component, OnInit } from '@angular/core';
import { Feedback } from 'src/app/models/feedback.model';
import { FeedbackService } from 'src/app/services/feedback/feedback.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { FeedbackFormComponent } from '../../dialog/feedback-form/feedback-form.component';
import { AuthUserService } from 'src/app/services/auth-user.service';

@Component({
  selector: 'app-feedback-list',
  templateUrl: './feedback-list.component.html',
  styleUrls: ['./feedback-list.component.css']
})
export class FeedbackListComponent implements OnInit {
  listLoaded = false;
  messageLoaded = true;
  feedbackList: Feedback[];
  currentFeedback: Feedback = new Feedback('null');
  messageFormControl: FormControl = new FormControl('', [
    Validators.required
  ]);
  isUserOperator = false;

  constructor(private auth: AuthUserService, private feedbackService: FeedbackService, private dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit() { this.updateMessageList(); this.isUserOperator = this.auth.accessLevel === AuthUserService.OPERATOR; }

  updateMessageList() {
    this.listLoaded = false;
    this.feedbackService.getFeedbackList().subscribe(data => {
      this.feedbackList = data;
      this.listLoaded = true;
    }, error => {
      this.showSnackMessage(error.error.message || error.message);
      this.listLoaded = true;
    });
  }

  onClickFeedback(feedback: Feedback) {
    this.messageLoaded = false;
    this.feedbackService.getFeedbackById(feedback.id).subscribe(data => {
      this.currentFeedback = data;
      this.messageLoaded = true;
    }, error => {
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  sendMessage() {
    const sendFeedback = this.currentFeedback;
    this.feedbackService.createFeedbackReply(sendFeedback, this.messageFormControl.value).subscribe(data => {
      if (data.code === 1) {
        this.messageFormControl.setValue('');
        this.messageFormControl.markAsUntouched();
        this.onClickFeedback(sendFeedback);
      } else {
        this.showSnackMessage(data.message);
      }
    }, error => {
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  openFeedback() {
    this.dialog.open(FeedbackFormComponent, {
      width: '90%',
      maxWidth: '500px',
      data: new Feedback('system')
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
