import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Vendor } from 'src/app/models/vendor.model';
import { MenuVendorListService } from 'src/app/services/menu/menu-vendor-list.service';
import { MatDialog, MatSnackBar, MatRadioChange } from '@angular/material';
import { Menu } from 'src/app/models/menu.model';
import { FormControl } from '@angular/forms';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { Order } from 'src/app/models/order.model';
import { OrderNewComponent } from '../../dialog/order-new/order-new.component';
import { Meal } from 'src/app/models/meal.model';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  vendorListLoaded = false;
  menuLoaded = false;
  vendorList: Vendor[] = [];
  vendor: Vendor = new Vendor();
  menu: Menu = new Menu();
  isLunch = false;
  index = 0;
  nearestOrderableDate: Date = Menu.getMinimumEditableDate();
  maxOrderableDate: Date = new Date(this.nearestOrderableDate.getTime());
  isNearestDate = true;
  isMaxDate = false;

  inOrderView = false;

  dateFormControl: FormControl = new FormControl(Menu.getMinimumEditableDate());

  constructor(public dialog: MatDialog, private menuVendorListHandler: MenuVendorListService, public orderService: OrderService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.maxOrderableDate.setMonth(this.maxOrderableDate.getMonth() + 1);
    this.menuVendorListHandler.getVendorList().subscribe(vendorList => {
      this.vendorListLoaded = true;
      this.vendorList = vendorList;
      this.vendor = this.vendorList[0] || this.vendor;
      if (this.vendorList[0]) {
        this.updateMenuDate(this.dateFormControl.value);
      }
    }, error => this.showSnackMessage(error.error.message || error.message));
  }

  peekVendor(step: number) {
    if (this.index <= 0 && step === -1) {
      this.index = this.vendorList.length - 1;
    } else if (this.index >= this.vendorList.length - 1 && step === 1) {
      this.index = 0;
    } else {
      this.index += step;
    }
    this.vendor = this.vendorList[this.index];
    this.updateMenuDate(this.dateFormControl.value);
  }

  changeMenuDate(plusminus: number) {
    const selDate = new Date(this.dateFormControl.value);
    selDate.setDate(selDate.getDate() + plusminus);
    const isoDate = selDate.toDateString();
    this.dateFormControl.setValue(selDate);
    this.updateMenuDate(isoDate);
  }

  updateMenuDate(datestr: string | Date) {
    const date: Date = datestr instanceof Date ? datestr : new Date(datestr + ' GMT+0');
    this.isNearestDate = (date.getTime() === this.nearestOrderableDate.getTime());
    this.isMaxDate = (date.getTime() === this.maxOrderableDate.getTime());
    if (date >= this.nearestOrderableDate && date <= this.maxOrderableDate) {
      this.menuLoaded = false;
      this.getMenuByDate(date, this.isLunch);
    }
  }

  updateMenuType(type: MatRadioChange) {
    this.isLunch = type.value === '1';
    this.updateMenuDate(this.dateFormControl.value.toDateString());
  }

  getOrderableMenu() {
    const today = this.nearestOrderableDate.toDateString();
    this.dateFormControl.setValue(this.nearestOrderableDate);
    this.updateMenuDate(today);
  }

  getMenuByDate(date: Date, isLunch?: boolean) {
    // implement backend services
    this.menu = new Menu();
    this.inOrderView = false;
    this.menuVendorListHandler.getMenuDetail(this.vendor, date, isLunch).subscribe(menu => {
      this.menu = menu;
      this.menu.vendor = this.vendor;
      this.menuLoaded = true;
    }, error => {
      this.dialog.open(ConfirmComponent, {
        width: '400px',
        data: {
          header: 'Error encountered!',
          body: error.error.message,
          action: 'OK'
        }
      });
      this.menu.vendor = this.vendor;
      this.menuLoaded = true;
    });
  }

  onClickMeal(meal: Meal) {
    this.showOrderRequestPopup(this.menu, meal);
  }

  showOrderRequestPopup(menu: Menu, meal: Meal) {
    const orderDetail = this.orderService.getOrderDetail(menu, meal) || new OrderDetail(meal, 0);
    const dialogRef = this.dialog.open(OrderNewComponent, {
      width: '300px',
      data: orderDetail
    });
    dialogRef.afterClosed().subscribe(qty => {
      if (qty >= 1 && qty <= 5) {
        orderDetail.quantity = qty;
        this.orderService.updateOrder(menu, orderDetail);
      }
    });
  }

  toggleOrderView() {
    this.inOrderView = !this.inOrderView;
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }

}
