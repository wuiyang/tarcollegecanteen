import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Meal } from 'src/app/models/meal.model';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.css']
})
export class MealComponent implements OnInit {
  @Input() mealDetail: Meal;
  @Output() clicked = new EventEmitter<Meal>();

  constructor() { }

  ngOnInit() {
  }

  showNewOrder() {
    this.clicked.emit(this.mealDetail);
  }
}
