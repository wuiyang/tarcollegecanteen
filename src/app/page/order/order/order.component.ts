import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';
import { OrderNewComponent } from '../../dialog/order-new/order-new.component';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { OrderService } from 'src/app/services/order/order.service';
import { CreditPointService } from 'src/app/services/user/credit-point.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  constructor(public dialog: MatDialog, public orderService: OrderService, private creditService: CreditPointService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  onClickEditDetail(orderIndex: number, orderDetailIndex: number) {
    this.showOrderRequestPopup(orderIndex, orderDetailIndex);
  }

  onClickDeleteDetail(orderIndex: number, orderDetailIndex: number) {
    const order = this.orderService.getOrder(orderIndex);
    this.dialog.open(ConfirmComponent, {
      width: '250px',
      data: {
        header: 'Delete preorder detail',
        body: 'Confirm delete preorder detail for date ' + order.menu.date.toDateString() + '?',
        cancel: 'No',
        action: 'Yes'
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        this.orderService.deleteOrderByIndex(order, orderDetailIndex);
      }
    });
  }

  showOrderRequestPopup(orderIndex: number, detailIndex: number, order?: Order, orderDetail?: OrderDetail) {
    if (order == null) {
      order = this.orderService.getOrder(orderIndex);
    }
    if (orderDetail == null) {
      orderDetail = order.details[detailIndex];
    }
    const dialogRef = this.dialog.open(OrderNewComponent, {
      width: '300px',
      data: orderDetail
    });
    dialogRef.afterClosed().subscribe(qty => {
      if (qty >= 1 && qty <= 5) {
        orderDetail.quantity = qty;
        this.updateOrder(order, orderDetail);
      }
    });
  }

  updateOrder(order: Order, orderDetail: OrderDetail) {
    this.orderService.updateOrder(order.menu, orderDetail);
  }

  askPlaceOrder(orderIndex: number) {
    const order: Order = this.orderService.getOrder(orderIndex);

    this.dialog.open(ConfirmComponent, {
      width: '400px',
      data: {
        header: 'Place order',
        body: 'Confirm to place preorder for date ' + order.menu.date.toDateString() + '?',
        bodyDescription: 'Once confirmed, credit points will be deducted from your account, below are transaction info to be made',
        detail: true,
        userId: order.menu.vendor.id + ' (Vendor account ID)',
        amount: order.getTotalPrice(),
        description: 'Payment for ' + order.menu.date.toDateString() + ' ' + (order.menu.isLunch ? 'lunch' : 'breakfast') + ' preorder',
        cancel: 'Cancel',
        action: 'Confirm'
      }
    }).afterClosed().subscribe(confirm => {
      if (confirm) {
        this.orderService.placeOrder(order).subscribe(data => {
          if (data.code === 1) {
            this.creditService.retrieveCreditInfo();
            this.orderService.deleteAllOrder(order);
            this.showSnackMessage(data.message);
          } else {
            this.showErrorPopup(data.message);
          }
        }, error => {
          this.showErrorPopup(error.error.message || error.message);
        });
      }
    });
  }

  showErrorPopup(errorMessage: string) {
    this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: {
        header: 'Error encountered!',
        body: errorMessage,
        action: 'OK'
      }
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
