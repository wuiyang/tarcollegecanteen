import { Component, OnInit, ViewChild } from '@angular/core';
import { CreditPointService } from 'src/app/services/user/credit-point.service';
import { MatDialog, MatSnackBar, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { CreditExchangeComponent } from '../../dialog/credit-exchange/credit-exchange.component';

@Component({
  selector: 'app-credit-top-up',
  templateUrl: './credit-top-up.component.html',
  styleUrls: ['./credit-top-up.component.css']
})
export class CreditTopUpComponent implements OnInit {
  @ViewChild(MatSort) sortTable: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  loaded = false;
  transactionDataSource = new MatTableDataSource([]);
  displayedColumns: string[] = ['transDate', 'transId', 'toUser', 'description', 'credit', 'debit'];

  constructor(public creditService: CreditPointService, private dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.retrieveTopup();
    this.transactionDataSource.sort = this.sortTable;
    this.transactionDataSource.paginator = this.paginator;
    this.transactionDataSource.sortingDataAccessor = (data, sortHeaderId) => {
      switch (sortHeaderId) {
        case 'transDate': return data.date.getTime();
        case 'transId': return data.id.toLowerCase();
        case 'toUser': return (data.fromUser === this.creditService.user ? data.toUser : data.fromUser).toLowerCase();
        case 'description': return data.description.toLowerCase();
        case 'credit': return (data.fromUser === this.creditService.user ? 0 : data.amount);
        case 'debit': return (data.fromUser === this.creditService.user ? data.amount : 0);
        default: return data.id;
      }
    };
  }

  retrieveTopup() {
    this.loaded = false;
    this.creditService.retrieveCreditTopupList().subscribe(data => {
      this.loaded = true;
      this.transactionDataSource.data = data;
    }, error => {
      this.loaded = true;
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  createNewExchange() {
    this.dialog.open(CreditExchangeComponent, { width: '350px' }).afterClosed().subscribe(exchanged => {
      if (exchanged) {
        this.retrieveTopup();
      }
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
