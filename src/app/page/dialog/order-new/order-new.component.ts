import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Validators, FormControl } from '@angular/forms';
import { OrderDetail } from 'src/app/models/order-detail.model';

@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.css']
})
export class OrderNewComponent implements OnInit {
  quantityFormControl = new FormControl('', [
    Validators.required,
    Validators.min(1),
    Validators.max(5)
  ]);

  constructor(public dialogRef: MatDialogRef<OrderNewComponent>, @Inject(MAT_DIALOG_DATA) public orderDetail: OrderDetail) { }

  ngOnInit() {
    this.quantityFormControl.setValue(this.orderDetail.quantity);
  }

  closeDialog() {
    const qty = this.quantityFormControl.value;
    if (qty >= 1 && qty <= 5) {
      this.dialogRef.close(qty);
    } else {
      this.quantityFormControl.markAsTouched();
    }
  }
}
