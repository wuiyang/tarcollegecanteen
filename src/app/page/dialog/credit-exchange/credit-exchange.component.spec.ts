import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditExchangeComponent } from './credit-exchange.component';

describe('CreditExchangeComponent', () => {
  let component: CreditExchangeComponent;
  let fixture: ComponentFixture<CreditExchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditExchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
