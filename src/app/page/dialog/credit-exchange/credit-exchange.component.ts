import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { CreditTransaction } from 'src/app/models/credit-transaction.model';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { CreditPointService } from 'src/app/services/user/credit-point.service';

@Component({
  selector: 'app-credit-exchange',
  templateUrl: './credit-exchange.component.html',
  styleUrls: ['./credit-exchange.component.css']
})
export class CreditExchangeComponent implements OnInit {
  serverErrorMessage = { userId: '', amount: '', popup: '' };
  uploading = false;
  isTopup = true;
  creditFormControl: FormControl = new FormControl(0, [
    Validators.required,
    Validators.min(10),
    Validators.max(2500)
  ]);
  rmFormControl: FormControl = new FormControl(0, [
    Validators.required,
    Validators.min(1),
    Validators.max(250)
  ]);

  userFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[0-9a-zA-Z]{10}')
  ]);

  transaction: CreditTransaction;

  constructor(public dialogRef: MatDialogRef<CreditExchangeComponent>, public creditService: CreditPointService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.creditFormControl.valueChanges.subscribe(credit => {
      if (this.rmFormControl.value !== credit / 10) {
        this.rmFormControl.setValue(credit / 10);
      }
    });
    this.rmFormControl.valueChanges.subscribe(rm => {
      if (this.creditFormControl.value !== rm * 10) {
        this.creditFormControl.setValue(rm * 10);
      }
    });
  }

  switchExchange() {
    this.isTopup = !this.isTopup;
  }

  submitExchange() {
    this.serverErrorMessage.popup = '';
    this.uploading = true;
    const userId = this.userFormControl.value;
    this.creditService.exchangeCredit(this.userFormControl.value, this.isTopup, this.creditFormControl.value).subscribe(
      data => {
        this.uploading = false;
        if (data.id) {
          this.showSnackMessage('Successfully exchanged money for user ID ' + userId);
          this.dialogRef.close(true);
        } else {
          this.serverErrorMessage.popup = data.message;
        }
      }, error => {
        this.uploading = false;
        if (error.error.code === 300) {
          error.error.message.forEach(errorMessage => {
            this.serverErrorMessage[errorMessage.type] = errorMessage.message;
            if (errorMessage.type === 'userId') {
              this.userFormControl.setErrors({ serverInvalid: true });
              this.userFormControl.markAsTouched();
            } else if (errorMessage.type === 'amount') {
              this.creditFormControl.setErrors({ serverInvalid: true });
              this.creditFormControl.markAsTouched();
            }
          });
        } else {
          this.serverErrorMessage.popup = error.error.message;
        }
      }
    );
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
