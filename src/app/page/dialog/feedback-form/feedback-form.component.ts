import { Component, OnInit, Inject } from '@angular/core';
import { Feedback } from 'src/app/models/feedback.model';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { FeedbackService } from 'src/app/services/feedback/feedback.service';

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.component.html',
  styleUrls: ['./feedback-form.component.css']
})
export class FeedbackFormComponent implements OnInit {
  serverErrorMessage = {message: '', popup: ''};
  messageFormControl: FormControl = new FormControl('', [
    Validators.minLength(10),
    Validators.required
  ]);

  constructor(public dialogRef: MatDialogRef<FeedbackFormComponent>, @Inject(MAT_DIALOG_DATA) public feedback: Feedback,
              private feedbackService: FeedbackService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  submitFeedback() {
    this.serverErrorMessage.popup = '';
    this.feedbackService.createFeedback(this.feedback, this.messageFormControl.value).subscribe(data => {
      if (data.id) {
        this.showSnackMessage(data.message);
        this.dialogRef.close();
      } else {
        this.serverErrorMessage.popup = data.message;
      }
    }, error => {
      const errorMessage = error.error.message;
      if (errorMessage) {
        error.message.forEach((element: { type: string; message: string; }) => {
          this.serverErrorMessage[element.type] = element.message;
          if (element.type === 'message') {
            this.messageFormControl.setErrors({ serverInvalid: true });
            this.messageFormControl.markAsTouched();
          }
        });
      } else {
        this.serverErrorMessage.popup = error.message;
      }
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
