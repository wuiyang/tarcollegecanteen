import { Component, OnInit } from '@angular/core';
import { Meal } from 'src/app/models/meal.model';
import { VendorMealService } from 'src/app/services/vendor/vendor-meal.service';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-vendor-menu-new-meal',
  templateUrl: './vendor-menu-new-meal.component.html',
  styleUrls: ['./vendor-menu-new-meal.component.css']
})
export class VendorMenuNewMealComponent implements OnInit {

  mealList: Meal[];
  loaded = false;
  selectedMeal: Meal;

  serverErrorMessage: any = { price: '' };

  mealSelectFormControl: FormControl = new FormControl('', [
    Validators.required
  ]);
  priceFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
    Validators.max(350)
  ]);

  constructor(public dialogRef: MatDialogRef<VendorMenuNewMealComponent>,
              private vendorMealHandler: VendorMealService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.vendorMealHandler.getMealList().subscribe(received => {
      this.mealList = received;
      this.selectedMeal = new Meal('');
      this.loaded = true;
    }, error => {
      this.showSnackMessage(error.error.message);
      this.loaded = true;
    });
  }

  changedMealSelection(meal: Meal) {
    this.priceFormControl.setValue(meal.price);
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
  closeDialog() {
    this.priceFormControl.markAsTouched();
    if (this.selectedMeal.id.length === 0) {
      this.mealSelectFormControl.markAsTouched();
    } else if (this.priceFormControl.value >= 0 && this.priceFormControl.value <= 350) {
      this.selectedMeal.price = this.priceFormControl.value;
      this.dialogRef.close(this.selectedMeal);
    }
  }
}
