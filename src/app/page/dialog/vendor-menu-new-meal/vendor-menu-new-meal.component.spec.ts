import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMenuNewMealComponent } from './vendor-menu-new-meal.component';

describe('VendorMenuNewMealComponent', () => {
  let component: VendorMenuNewMealComponent;
  let fixture: ComponentFixture<VendorMenuNewMealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMenuNewMealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMenuNewMealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
