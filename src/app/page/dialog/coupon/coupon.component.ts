import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Order } from 'src/app/models/order.model';
import { CreditPointService } from 'src/app/services/user/credit-point.service';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.css']
})
export class CouponComponent implements OnInit {
  stringifiedJsonData: string;
  displayedColumns: string[] = ['name', 'quantity'];

  constructor(public dialogRef: MatDialogRef<CouponComponent>, @Inject(MAT_DIALOG_DATA) public order: Order,
              public creditService: CreditPointService) { }

  ngOnInit() {
    this.stringifiedJsonData = '{' +
        '"id":"' + this.order.id + '",' +
        '"menuId":"' + this.order.menu.id + '",' +
        '"userId":"' + this.creditService.user + '",' +
        '"details":{';
    this.order.details.forEach((detail, index) => {
      this.stringifiedJsonData += '"' + detail.meal.id + '":' + detail.quantity + '';
      if (index !== this.order.details.length - 1) {
        this.stringifiedJsonData += ',';
      }
    });
    this.stringifiedJsonData += '}}';
  }

}
