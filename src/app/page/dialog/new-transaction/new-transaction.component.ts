import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CreditPointService } from 'src/app/services/user/credit-point.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-new-transaction',
  templateUrl: './new-transaction.component.html',
  styleUrls: ['./new-transaction.component.css']
})
export class NewTransactionComponent implements OnInit {
  uploading = false;
  formData: FormGroup;

  serverErrorMessage = {userId: '', amount: '', other: ''};

  userIdFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9a-zA-Z]{10}$')
  ]);

  amountFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.min(1),
    Validators.max(2500)
  ]);

  descriptionFormControl: FormControl = new FormControl('');
  typeFormControl: FormControl = new FormControl('');

  constructor(public dialogRef: MatDialogRef<NewTransactionComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
              public creditService: CreditPointService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.formData = new FormGroup({
      userId: this.userIdFormControl,
      amount: this.amountFormControl,
      description: this.descriptionFormControl
    }, { updateOn: 'blur' });
    this.typeFormControl.setValue('0');
    this.formData.valueChanges.subscribe(() => this.generateQrCodeMessage());
  }

  closeDialog() {
    this.uploading = true;
    let observableListener: Observable<any>;
    const isSend = this.typeFormControl.value === '0';
    if (isSend) {
      observableListener = this.creditService.sendCredit(this.userIdFormControl.value, this.amountFormControl.value,
                                                         this.descriptionFormControl.value);
    } else {
      observableListener = this.creditService.requestCredit(this.userIdFormControl.value, this.amountFormControl.value,
                                                            this.descriptionFormControl.value);
    }
    observableListener.subscribe(data => {
      if (data.id) {
        this.showSnackMessage('Successfully ' + (isSend ? 'send' : 'request') + ' ' + this.amountFormControl.value + ' credit points!');
        this.creditService.retrieveCreditInfo();
        this.dialogRef.close(true);
      } else {
        this.serverErrorMessage.other = data.message;
      }
      this.uploading = false;
    }, error => {
      if (error.error.code === 300) {
        error.error.message.forEach(errorMessage => {
          this.serverErrorMessage[errorMessage.type] = errorMessage.message;
          this.formData.get(errorMessage.type).setErrors({ serverInvalid: true });
          this.formData.get(errorMessage.type).markAsTouched();
        });
      } else {
        this.serverErrorMessage.other = error.error.message;
      }
      this.uploading = false;
    });
  }

  generateQrCodeMessage() {
    const qrData: any = { toUser: this.creditService.user };
    qrData.fromUser = this.userIdFormControl.value;
    qrData.amount = this.amountFormControl.value;
    qrData.description = this.descriptionFormControl.value;
    this.serverErrorMessage.other = '';
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
