import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { Menu } from 'src/app/models/menu.model';
import { OrderService } from 'src/app/services/order/order.service';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { MatDialog, MatSnackBar, MatTable } from '@angular/material';
import { CreditPointService } from 'src/app/services/user/credit-point.service';
import { CouponComponent } from '../../dialog/coupon/coupon.component';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { FeedbackFormComponent } from '../../dialog/feedback-form/feedback-form.component';
import { Feedback } from 'src/app/models/feedback.model';

@Component({
  selector: 'app-order-history-detail',
  templateUrl: './order-history-detail.component.html',
  styleUrls: ['./order-history-detail.component.css']
})
export class OrderHistoryDetailComponent implements OnInit {
  @Input() order: Order;
  @Input() orderIndex: number;
  @Output() cancelOrder: EventEmitter<Order> = new EventEmitter();
  @ViewChild(MatTable) table: MatTable<any>;
  backup: OrderDetail[];

  editable = false;
  isEditing = false;
  displayedColumns: string[] = ['picture', 'name', 'price', 'quantity', 'total'];
  formGroup: FormGroup = new FormGroup({});

  constructor(public dialog: MatDialog, private creditService: CreditPointService,
              private orderService: OrderService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.editable = this.order.menu.date >= Menu.getMinimumCancelableDate();
    if (this.editable) {
      this.createBackup();
      this.order.details.forEach(detail => {
        this.formGroup.addControl(detail.meal.id, new FormControl(detail.quantity, [
          Validators.required,
          Validators.min(1),
          Validators.max(5)
        ]));
      });
    }
  }

  createBackup() {
    this.backup = [];
    this.order.details.forEach((detail: OrderDetail) => {
      this.backup.push(new OrderDetail(detail.meal, detail.quantity));
    });
  }

  loadBackup() {
    this.order.details = [];
    this.backup.forEach((detail: OrderDetail) => {
      this.order.details.push(new OrderDetail(detail.meal, detail.quantity));
    });
  }

  askCancelOrder() {
    this.dialog.open(ConfirmComponent, {
      width: '250px',
      data: {
        header: 'Cancel order?',
        body: 'Confirm to cancel order for date ' + this.order.menu.date.toDateString() + '?',
        cancel: 'No',
        action: 'Yes'
      }
    }).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      this.orderService.cancelOrder(this.order).subscribe((data) => {
        this.creditService.retrieveCreditInfo();
        if (data.code === 1) {
          this.showSnackMessage(data.message);
          this.cancelOrder.emit(this.order);
        } else {
          this.showErrorPopup(data.message);
        }
      }, error => {
        this.showErrorPopup(error.error.message || error.message);
      });
    });
  }

  askConfirmEdit() {
    if (this.order.details.length === 0) {
      return this.askCancelOrder();
    }

    this.dialog.open(ConfirmComponent, {
      width: '250px',
      data: {
        header: 'Confirm modify order?',
        body: 'Confirm to modify order for date ' + this.order.menu.date.toDateString() + '?',
        cancel: 'No',
        action: 'Yes'
      }
    }).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      this.orderService.modifyOrder(this.order).subscribe(data => {
        this.creditService.retrieveCreditInfo();
        if (data.code === 1 && data.id) {
          this.createBackup();
          this.cancelEdit(true);
          this.showSnackMessage(data.message);
        } else {
          this.cancelEdit();
          this.showErrorPopup(data.message);
        }
        this.isEditing = false;
      }, error => {
        this.cancelEdit();
        this.showErrorPopup(error.error.message || error.message);
      });
    });
  }

  showCoupon() {
    this.dialog.open(CouponComponent, {
      minWidth: '250px',
      maxWidth: '900px',
      maxHeight: '80vh',
      data: this.order
    });
  }

  onQuantityChange(detailIndex: number) {
    const detail = this.order.details[detailIndex];
    detail.quantity = this.formGroup.get(detail.meal.id).value;
  }

  removeDetailFromOrder(detailIndex: number) {
    this.order.details.splice(detailIndex, 1);
    this.table.renderRows();
  }

  changeEditView() {
    if (this.displayedColumns.length === 5) {
      this.displayedColumns.push('action');
    }
    this.isEditing = true;
  }

  cancelEdit(noload?: boolean) {
    if (this.displayedColumns.length === 6) {
      this.displayedColumns.pop();
    }
    if (!noload) {
      this.loadBackup();
    }
    this.isEditing = false;
  }

  showErrorPopup(errorMessage: string) {
    this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: {
        header: 'Error encountered!',
        body: errorMessage,
        action: 'OK'
      }
    });
  }

  showFeedbackForm() {
    this.dialog.open(FeedbackFormComponent, {
      width: '90%',
      maxWidth: '500px',
      data: new Feedback(this.order.menu.vendor.name, null, this.order.id)
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
