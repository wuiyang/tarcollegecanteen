import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { OrderService } from 'src/app/services/order/order.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {
  loaded = false;
  orderList: Order[] = [];
  lastLoadedType = '0';

  constructor(private orderService: OrderService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getOrderList('0');
  }

  getOrderList(type: string) {
    this.lastLoadedType = type;
    this.loaded = false;
    this.orderService.getOrderList(type !== '0', type !== '-1').subscribe(data => {
      this.orderList = data;
      this.loaded = true;
    }, error => {
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  onCancelOrder(order: Order) {
    this.getOrderList(this.lastLoadedType);
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
