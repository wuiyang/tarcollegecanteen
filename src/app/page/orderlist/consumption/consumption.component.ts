import { Component, OnInit, Directive } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OrderService } from 'src/app/services/order/order.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-consumption',
  templateUrl: './consumption.component.html',
  styleUrls: ['./consumption.component.css']
})
export class ConsumptionComponent implements OnInit {
  isLoaded = false;
  minShowableDate: Date;
  dateFormControl: FormControl = new FormControl(new Date());
  typeFormControl: FormControl = new FormControl('1');
  dateConsumptionArray: {date: Date, isLunch: boolean, consumption: number}[];
  total = 0;
  average = 0;
  dayAverage = 0;

  chosenDate = new Date();
  chosenType = '1';

  displayedColumns: string[] = ['date', 'type', 'consumption'];

  constructor(private orderService: OrderService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.updateReportDate();
  }

  updateChosenDate(date: Date) {
    this.chosenDate = date;
    this.updateReportDate();
  }

  updateChosenType(type: string) {
    this.chosenType = type;
    this.updateReportDate();
  }

  updateReportDate() {
    let type: string;
    switch (this.chosenType) {
      case '0': type = 'day'; break;
      case '1': type = 'week'; break;
      case '2': type = 'month'; break;
    }

    this.isLoaded = false;
    this.orderService.getConsumptionData(this.chosenDate, type).subscribe(data => {
      this.dateConsumptionArray = data.dateConsumptionArray;
      this.total = data.total;
      this.average = data.count ? parseFloat((data.total / data.count).toFixed(2)) : 0;
      this.dayAverage = data.dayCount > 0 ? parseFloat((data.total / data.dayCount).toFixed(2)) : 0;
      this.isLoaded = true;
    }, error => {
      this.isLoaded = true;
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }

}
