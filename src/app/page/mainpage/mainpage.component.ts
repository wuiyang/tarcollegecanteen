import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'src/app/services/auth-user.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  readonly ANONYMOUS = AuthUserService.ANONYMOUS;
  readonly CUSTOMER = AuthUserService.CUSTOMER;
  readonly VENDOR = AuthUserService.VENDOR;
  readonly OPERATOR = AuthUserService.OPERATOR;

  constructor(public auth: AuthUserService) { }

  ngOnInit() {
  }

}
