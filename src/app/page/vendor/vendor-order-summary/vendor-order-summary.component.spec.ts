import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorOrderSummaryComponent } from './vendor-order-summary.component';

describe('VendorOrderSummaryComponent', () => {
  let component: VendorOrderSummaryComponent;
  let fixture: ComponentFixture<VendorOrderSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorOrderSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorOrderSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
