import { Component, OnInit, Input } from '@angular/core';
import { Menu } from 'src/app/models/menu.model';
import { Meal } from 'src/app/models/meal.model';

@Component({
  selector: 'app-vendor-order-summary',
  templateUrl: './vendor-order-summary.component.html',
  styleUrls: ['./vendor-order-summary.component.css']
})
export class VendorOrderSummaryComponent implements OnInit {
  @Input() menu: Menu;
  @Input() mealOrderQuantity: any;
  showData: Meal[] = [];
  displayTotal = 0;
  displayedColumns: string[] = ['picture', 'name', 'price', 'quantity', 'subtotal'];

  constructor() { }

  ngOnInit() {
    this.showData = this.menu.meals.filter((meal: Meal) => this.mealOrderQuantity[meal.id]);
    this.calculateTotal();
  }

  calculateTotal() {
    let total = 0;
    this.menu.meals.forEach(meal => {
      total += meal.price * (this.mealOrderQuantity[meal.id] || 0);
    });
    this.displayTotal = total;
  }

}
