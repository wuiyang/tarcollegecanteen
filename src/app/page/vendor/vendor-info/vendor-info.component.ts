import { Component, OnInit, Input } from '@angular/core';
import { Vendor } from 'src/app/models/vendor.model';
import { FormControl, Validators } from '@angular/forms';
import { PictureService } from 'src/app/services/user/picture.service';
import { Observable } from 'rxjs';
import { HttpEventType } from '@angular/common/http';
import { MatDialog, MatSnackBar } from '@angular/material';
import { VendorInfoService } from 'src/app/services/vendor/vendor-info.service';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';
import { Url } from 'src/app/models/geturl.model';

@Component({
  selector: 'app-vendor-info',
  templateUrl: './vendor-info.component.html',
  styleUrls: ['./vendor-info.component.css']
})
export class VendorInfoComponent implements OnInit {
  @Input() vendor: Vendor;
  serverErrorMessage = { name: '', description: '' };
  nameFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.min(4)
  ]);
  descriptionFormControl: FormControl = new FormControl('');

  oldPictureId = '';
  oldPictureUrl = '';
  fileUploading = false;
  fileProgress = 0;

  constructor(private pictureHandler: PictureService, private vendorInfoHandler: VendorInfoService,
              private snackBar: MatSnackBar, public dialog: MatDialog) { }

  ngOnInit() {
  }

  editVendorInfo() {
    this.oldPictureId = this.vendor.pictureId;
    this.oldPictureUrl = this.vendor.pictureUrl;
    this.nameFormControl.setValue(this.vendor.name);
    this.descriptionFormControl.setValue(this.vendor.description);
    this.vendor.editable = true;
  }

  saveVendorInfo() {
    this.vendor.name = this.nameFormControl.value;
    this.vendor.description = this.descriptionFormControl.value;
    this.uploadVendorInfo().subscribe(result => {
      this.vendor.editable = false;
      this.vendor.hasInfo = true;
      this.vendor.id = result.id;
      this.oldPictureId = this.vendor.pictureId;
      this.oldPictureUrl = this.vendor.pictureUrl;
      this.showSnackMessage(result.message);
    }, error => {
      this.errorToDisplay(Url.getErrorMessage(error));
    });
  }

  onFileChange(fileInput) {
    const selectedFile = fileInput.target.files[0];
    if (selectedFile == null) {
      return;
    }
    this.fileUploading = true;
    this.pictureHandler.uploadPicture(selectedFile).then((httpPosted: Observable<any>) => {
      httpPosted.subscribe((event) => this.fileUploadingEvent(event), (error) => this.fileReceiveError(error));
    }).catch((error) => this.fileReceiveError(error));
  }

  fileUploadingEvent(event) {
    if (event.type === HttpEventType.UploadProgress) {
      this.fileProgress = 100 * event.loaded / event.total;
    } else if (event.body && event.body.id) {
      this.vendor.pictureId = event.body.id;
      this.vendor.pictureUrl = event.body.url;
      this.fileUploading = false;
    }
  }

  fileReceiveError(error) {
    this.dialog.open(ConfirmComponent, {
      width: '300px',
      data: {
        header: 'Error received!',
        body: error.error.message || error.message,
        action: 'OK'
      }
    });
  }

  discardVendorInfo() {
    this.vendor.editable = false;
    this.resetInputValue();
  }

  errorToDisplay(error: any) {
    if (error.code === 300) { // custom error message
      error.message.forEach((element: { type: string; message: string; }) => {
        this.serverErrorMessage[element.type] = element.message;
        const formElement = element.type === 'name' ? this.nameFormControl : this.descriptionFormControl;
        formElement.setErrors({ serverInvalid: true });
        formElement.markAsTouched();
      });
    } else {
      this.showSnackMessage(error.error.message || error.message);
    }
  }

  private uploadVendorInfo(): Observable<any> {
    return this.vendorInfoHandler.uploadVendorInfo(this.vendor);
  }

  private resetInputValue() {
    this.nameFormControl.setValue(this.vendor.name);
    this.descriptionFormControl.setValue(this.vendor.description);
    this.vendor.pictureId = this.oldPictureId;
    this.vendor.pictureUrl = this.oldPictureUrl;
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
