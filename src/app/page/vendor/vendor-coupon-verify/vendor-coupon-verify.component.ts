import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { VendorOrderService } from 'src/app/services/vendor/vendor-order.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Order } from 'src/app/models/order.model';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';

@Component({
  selector: 'app-vendor-coupon-verify',
  templateUrl: './vendor-coupon-verify.component.html',
  styleUrls: ['./vendor-coupon-verify.component.css']
})
export class VendorCouponVerifyComponent implements OnInit {
  @ViewChild(ZXingScannerComponent) scanner: ZXingScannerComponent;

  enableScanner = true;
  deviceChosen;
  deviceList = [];
  message: string;
  order: Order;
  difference: any;
  differenceLength = -1;
  isApproving = false;
  private scannedJson;

  displayedColumns = ['name', 'quantity'];

  cameraFormControl: FormControl = new FormControl();

  constructor(public dialog: MatDialog, private vendorOrderHandler: VendorOrderService, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  foundCamera(videoDevice: any[]) {
    this.deviceList = videoDevice;
    if (videoDevice.length > 0) {
      this.cameraFormControl.setValue(videoDevice[0]);
      this.onSelectCamera(videoDevice[0]);
    }
  }

  onSelectCamera(camera) {
    this.deviceChosen = camera;
  }

  onClickScan() {
    this.message = null;
    this.order = null;
    this.difference = null;
    this.differenceLength = -1;
    this.scannedJson = null;
    this.enableScanner = true;
  }

  onSuccessScanQRCode(result: string) {
    let orderJson;
    try {
      orderJson = JSON.parse(result);
    } catch (error) {
      this.message = 'Invalid coupon! This coupon does not contain any information to verify order!';
    }
    this.enableScanner = false;
    this.scannedJson = orderJson;
    this.verifyOrderCoupon(orderJson);
  }

  approvePreorder() {
    this.isApproving = true;
    this.vendorOrderHandler.approvePreorder(this.scannedJson).subscribe(data => {
      this.isApproving = false;
      if (data.code === 1) {
        this.dialog.open(ConfirmComponent, {
          width: '250px',
          data: {
            header: 'Successfully updated preorder info.',
            body: data.message,
            action: 'OK'
          }
        });
        this.onClickScan();
      }
    }, error => {
      this.isApproving = false;
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  private verifyOrderCoupon(orderJson) {
    this.vendorOrderHandler.verifyOrder(orderJson).subscribe(data => {
      if (data.code !== 300) {
        this.order = data.order;
        this.difference = data.difference;
        this.differenceLength = Object.keys(this.difference).length;
      }
      this.message = data.message;
    }, error => {
      this.message = error.error.message || error.message;
    });
  }

  hasPermission(perm: boolean) {
    if (!perm) {
      this.showSnackMessage('Camera is required to scan and verify coupon\'s QR code.');
    }
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
