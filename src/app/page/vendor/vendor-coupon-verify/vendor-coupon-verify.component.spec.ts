import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorCouponVerifyComponent } from './vendor-coupon-verify.component';

describe('VendorCouponVerifyComponent', () => {
  let component: VendorCouponVerifyComponent;
  let fixture: ComponentFixture<VendorCouponVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorCouponVerifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorCouponVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
