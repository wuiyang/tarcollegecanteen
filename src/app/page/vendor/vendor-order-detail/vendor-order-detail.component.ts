import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/models/order.model';

@Component({
  selector: 'app-vendor-order-detail',
  templateUrl: './vendor-order-detail.component.html',
  styleUrls: ['./vendor-order-detail.component.css']
})
export class VendorOrderDetailComponent implements OnInit {
  @Input() order: Order;
  @Input() filterCancel: boolean;
  displayedColumns: string[] = ['picture', 'name', 'price', 'quantity', 'subtotal'];

  constructor() { }

  ngOnInit() {

  }

}
