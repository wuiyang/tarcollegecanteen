import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/models/menu.model';
import { FormControl } from '@angular/forms';
import { VendorMenuService } from 'src/app/services/vendor/vendor-menu.service';
import { Meal } from 'src/app/models/meal.model';
import { Observable } from 'rxjs';
import { MatSnackBar, MatDialog, MatRadioChange, MatCalendarCellCssClasses } from '@angular/material';
import { VendorMenuNewMealComponent } from '../../dialog/vendor-menu-new-meal/vendor-menu-new-meal.component';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';
import { VendorInfoService } from 'src/app/services/vendor/vendor-info.service';
import { Vendor } from 'src/app/models/vendor.model';

@Component({
  selector: 'app-vendor-menu',
  templateUrl: './vendor-menu.component.html',
  styleUrls: ['./vendor-menu.component.css']
})
export class VendorMenuComponent implements OnInit {
  loaded = false;
  inDefaultView = false;
  vendor: Vendor = new Vendor();
  menu: Menu = new Menu('', this.vendor);
  isLunch = false;
  dateFormControl: FormControl = new FormControl(Menu.getMinimumEditableDate());
  dayFormControl: FormControl = new FormControl('1');
  minShowableDate: Date;
  isMin = false;
  private customMenuDate: Date[];

  constructor(public dialog: MatDialog, private vendorMenuHandler: VendorMenuService,
              private vendorInfoHandler: VendorInfoService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.minShowableDate = new Date();
    this.minShowableDate.setMonth(this.minShowableDate.getMonth() - 1);
    this.vendorInfoHandler.getFullVendorInfo().then((vendor: Vendor) => {
      this.vendor = vendor;
      this.menu.vendor = this.vendor;
      this.loaded = true;
      if (this.vendor.hasInfo) {
        this.updateMenuDate(this.dateFormControl.value);
      }
    }).catch(errorReceived => {
      const error = errorReceived.error || errorReceived;
      if (error.code !== 101) {
        this.showSnackMessage(error.message);
      }
      this.loaded = true;
      this.vendor.editable = true;
    });

    this.vendorMenuHandler.getAllMenuDate().subscribe(data => {
      this.customMenuDate = data;
    });
  }

  dateClass = (date: Date): MatCalendarCellCssClasses => {
    const isInRange = this.customMenuDate.some(customDate =>
      (customDate.getUTCDate() === date.getDate() &&
      customDate.getUTCMonth() === date.getMonth() &&
      customDate.getUTCFullYear() === date.getFullYear()));
    return isInRange ? 'has-custom-menu' : undefined;
  }

  changeMenuDate(plusminus: number) {
    const selDate = new Date(this.dateFormControl.value);
    selDate.setDate(selDate.getDate() + plusminus);
    const isoDate = selDate.toISOString();
    this.dateFormControl.setValue(selDate);
    this.updateMenuDate(isoDate);
  }

  updateMenuType(type: MatRadioChange) {
    this.isLunch = type.value === '1';
    if (this.inDefaultView) {
      this.getDefaultMenu(this.dayFormControl.value);
    } else {
      this.updateMenuDate(this.dateFormControl.value.toDateString());
    }
  }

  getDefaultMenu(day: number) {
    if (day === -1) {
      if (this.inDefaultView) {
        this.inDefaultView = false;
        this.updateMenuDate(this.dateFormControl.value);
        return;
      }
      day = 1;
    }
    this.updateMenuDate('1970-01-' + ('0' + (+day + 4)).slice(-2));
  }

  getEditableMenu() {
    const todayDate = Menu.getMinimumEditableDate();
    const today = todayDate.toISOString().substr(0, 10);
    this.dateFormControl.setValue(todayDate);
    this.updateMenuDate(today);
  }

  resetMenuList() {
    this.vendorMenuHandler.deleteMenuInfo(this.menu).subscribe(data => {
      if (data.code === 1) {
        this.updateMenuDate(this.dateFormControl.value);
      }
      this.showSnackMessage(data.message);
    }, error => {
      this.showSnackMessage(error.error.message);
    });
  }

  updateMenuDate(datestr: string) {
    // waiting/process stuff goes here
    this.inDefaultView = (datestr.indexOf && datestr.indexOf('1970-01-') === 0);
    const date: Date = new Date(datestr);
    this.isMin = date < this.minShowableDate;
    this.loaded = false;
    this.getMenuByDate(date);
  }

  getMenuByDate(date: Date) {
    // implement backend services
    this.menu = new Menu();
    this.vendorMenuHandler.getMenuInfo(date, this.isLunch).subscribe(menu => {
      this.menu = menu;
      this.menu.vendor = this.vendor;
      this.loaded = true;
    }, error => {
      this.dialog.open(ConfirmComponent, {
        width: '400px',
        data: {
          header: 'Error encountered!',
          body: error.error.message,
          action: 'OK'
        }
      });
      this.menu.vendor = this.vendor;
      this.loaded = true;
    });
  }

  addNewMeal() {
    const dialogRef = this.dialog.open(VendorMenuNewMealComponent, {
      width: '400px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.id.length > 0) {
        if (this.menu.meals.find(meal => meal.id === result.id)) {
          this.dialog.open(ConfirmComponent, {
            width: '300px',
            data: {
              header: 'Meal exist',
              body: 'The selected meal exists in the menu meal list!',
              action: 'OK'
            }
          });
        } else {
          this.menu.meals.push(result);
          this.uploadMenuInfo(this.menu).subscribe(data => {
            this.menu.id = data.id;
            this.showSnackMessage(data.message);
          }, error => {
            let message = '';
            error.error.message.forEach(errorobj => {
              message += errorobj.message;
            });

            this.menu.meals.splice(this.menu.meals.indexOf(result), 1);
            this.dialog.open(ConfirmComponent, {
              width: '300px',
              data: {
                header: 'Error encountered!',
                body: message,
                action: 'OK'
              }
            });
          });
        }
      }
    });
  }

  onModified(meal: Meal) {
    let tempMeal: Meal;
    let tempIndex: number;
    if (meal.id.length === 0) {
      tempIndex = this.menu.meals.indexOf(meal);
      tempMeal = this.menu.meals.splice(tempIndex, 1)[0];
    }

    this.uploadMenuInfo(this.menu).subscribe(data => {
      this.menu.id = data.id;
      meal.editable = false;
      this.showSnackMessage(data.message);
    }, error => {
      if (tempMeal) {
        this.menu.meals.splice(tempIndex, 0, tempMeal);
      }
      this.showSnackMessage(error.error.message);
    });
  }

  uploadMenuInfo(menu: Menu): Observable<any> {
    return this.vendorMenuHandler.uploadMenuInfo(menu);
  }
  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
