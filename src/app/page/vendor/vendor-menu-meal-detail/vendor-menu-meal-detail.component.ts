import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Meal } from 'src/app/models/meal.model';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';

@Component({
  selector: 'app-vendor-menu-meal-detail',
  templateUrl: './vendor-menu-meal-detail.component.html',
  styleUrls: ['./vendor-menu-meal-detail.component.css']
})
export class VendorMenuMealDetailComponent implements OnInit {
  @Input() mealDetail: Meal;
  @Input() modifiable: boolean;
  @Output() modified = new EventEmitter<Meal>();

  serverErrorMessage: any = { price: '' };

  priceFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
    Validators.max(350)
  ]);

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.priceFormControl.setValue(this.mealDetail.price);
  }

  editMealInfo() {
    this.mealDetail.editable = true;
  }

  saveMealInfo() {
    this.mealDetail.price = this.priceFormControl.value;
    this.modified.emit(this.mealDetail);
  }

  discardMealInfo() {
    this.priceFormControl.setValue(this.mealDetail.price);
    this.mealDetail.editable = false;
  }

  askRemoveMealInfo() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '300px',
      data: {
        header: 'Delete meal from menu',
        body: 'Are you sure you want to remove this meal from menu?',
        cancel: 'No',
        action: 'Yes'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removeMealInfo();
      }
    });
  }
  removeMealInfo() {
    this.mealDetail.id = '';
    this.modified.emit(this.mealDetail);
  }
}
