import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMenuMealDetailComponent } from './vendor-menu-meal-detail.component';

describe('VendorMenuMealDetailComponent', () => {
  let component: VendorMenuMealDetailComponent;
  let fixture: ComponentFixture<VendorMenuMealDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMenuMealDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMenuMealDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
