import { Component, OnInit } from '@angular/core';
import { VendorOrderService } from 'src/app/services/vendor/vendor-order.service';
import { Menu } from 'src/app/models/menu.model';
import { MatSnackBar } from '@angular/material';
import { Order } from 'src/app/models/order.model';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-vendor-order',
  templateUrl: './vendor-order.component.html',
  styleUrls: ['./vendor-order.component.css']
})
export class VendorOrderComponent implements OnInit {
  menuLoaded = false;
  orderLoaded = false;
  menuList: Menu[] = [];
  orderList: Order[] = [];
  mealOrderQuantity = {}; // hashmap countData[id] = quantity
  loadedMenu: Menu = new Menu();
  menuSelectFormControl: FormControl = new FormControl();
  reportTypeFormControl: FormControl = new FormControl('0');

  reportType = '0';

  constructor(private vendorOrderHandler: VendorOrderService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.vendorOrderHandler.getMenuWithOrderList().subscribe(data => {
      this.menuList = data;
      this.menuLoaded = true;
      if (this.menuList.length > 0) {
        const index = this.locationOf(new Date(), this.menuList, (a: Date, bmenu: Menu) => {
          const b = bmenu.date;
          const diffYear = b.getUTCFullYear() - a.getFullYear();
          const diffMonth = b.getUTCMonth() - a.getMonth();
          const diffDate = b.getUTCDate() - a.getDate();

          // if same date
          if (diffYear === 0 && diffMonth === 0 && diffDate === 0) { return 0; }
          // if year is larger, or same year but month is larger, or same month but date is larger
          if (diffYear > 0 || (diffYear === 0 && (diffMonth > 0 || (diffMonth === 0 && diffDate > 0)))) { return -1; } else { return 1; }
        });
        this.menuSelectFormControl.setValue(this.menuList[index]);
        this.onSelectMenuDate(this.menuList[index]);
      } else {
        this.orderLoaded = true;
      }
    }, error => {
      this.menuLoaded = this.orderLoaded = true;
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  onSelectMenuDate(menu: Menu) {
    this.orderLoaded = false;
    this.loadedMenu = menu;
    this.vendorOrderHandler.getOrderListByMenuId(menu).subscribe(data => {
      this.orderList = data;
      this.countTotalFoodOrder();
      this.orderLoaded = true;
    }, error => {
      this.orderLoaded = true;
      this.showSnackMessage(error.error.message || error.message);
    });
  }

  onSelectReportType(type: string) {
    this.reportType = type;
  }

  countTotalFoodOrder() {
    const mealCount = {};
    this.orderList.forEach(order => {
      order.details.forEach(detail => {
        if (mealCount[detail.meal.id] == null) {
          mealCount[detail.meal.id] = 0;
        }
        if (!order.cancel) {
          mealCount[detail.meal.id] += detail.quantity;
        }
      });
    });
    this.mealOrderQuantity = mealCount;
  }

  private locationOf(element: any, array: any[], comparer: (a: any, b: any) => number, start?: number, end?: number): number {
    if (array.length === 0) {
        return -1;
    }

    start = start || 0;
    end = end || array.length;
// tslint:disable-next-line: no-bitwise
    const pivot = (start + end) >> 1;  // should be faster than dividing by 2

    const c = comparer(element, array[pivot]);
    if (end - start <= 1) { return c < 0 ? pivot - 1 : pivot; }

    if (c < 0) { return this.locationOf(element, array, comparer, start, pivot); }
    if (c > 0) { return this.locationOf(element, array, comparer, pivot, end); }
    return pivot;
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
