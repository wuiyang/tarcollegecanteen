import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMealDetailComponent } from './vendor-meal-detail.component';

describe('VendorMealDetailComponent', () => {
  let component: VendorMealDetailComponent;
  let fixture: ComponentFixture<VendorMealDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMealDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMealDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
