import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Meal } from 'src/app/models/meal.model';
import { VendorMealService } from 'src/app/services/vendor/vendor-meal.service';
import { Observable } from 'rxjs';
import { Url } from 'src/app/models/geturl.model';
import { MatSnackBar, MatDialog, MatTable } from '@angular/material';
import { ConfirmComponent } from '../../dialog/confirm/confirm.component';
import { PictureService } from 'src/app/services/user/picture.service';
import { HttpEventType } from '@angular/common/http';
import { Ingredient } from 'src/app/models/ingredient.model';

@Component({
  selector: 'app-vendor-meal-detail',
  templateUrl: './vendor-meal-detail.component.html',
  styleUrls: ['./vendor-meal-detail.component.css']
})
export class VendorMealDetailComponent implements OnInit {
  @Input() mealDetail: Meal;
  @Output() deleted = new EventEmitter<Meal>();

  @ViewChild(MatTable) table: MatTable<any>;

  formData: FormGroup;
  serverErrorMessage: any = {name: '', description: '', refPrice: '', ingredients: '', calories: ''};
  fileUploading = false;
  fileProgress = 0;
  oldPictureUrl: string;
  oldPictureId: string;
  oldIngredients: Ingredient[];
  separatorKeysCodes: number[] = [ENTER, COMMA];
  displayedColumns: string[] = ['name', 'quantity', 'action'];

  nameFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3)
  ]);
  descriptionFormControl: FormControl = new FormControl('');
  refPriceFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
    Validators.max(350)
  ]);
  caloriesFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
    Validators.max(1800)
  ]);
  ingredientsFormArray: FormArray;

  constructor(private vendorMealHandler: VendorMealService, private pictureHandler: PictureService,
              private snackBar: MatSnackBar, public dialog: MatDialog) { }

  ngOnInit() {
    this.ingredientsFormArray = new FormArray([]);
    this.formData = new FormGroup({
      name: this.nameFormControl,
      description: this.descriptionFormControl,
      refPrice: this.refPriceFormControl,
      calories: this.caloriesFormControl,
      ingredients: this.ingredientsFormArray
    });

    if (this.mealDetail.ingredients.length === 0) {
      this.mealDetail.ingredients.push(new Ingredient());
    }
    while (this.ingredientsFormArray.length < this.mealDetail.ingredients.length) {
      this.ingredientsFormArray.push(this.getIngredientFormGroup());
    }

    this.oldPictureId = this.mealDetail.pictureId;
    this.oldPictureUrl = this.mealDetail.pictureUrl;
    this.copyIngredients();
    this.resetInputValue();
  }

  editMealInfo() {
    this.mealDetail.editable = true;
  }

  askRemoveMealInfo() {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '300px',
      data: {
        header: 'Delete meal',
        body: 'Are you sure you want to remove this meal?',
        cancel: 'No',
        action: 'Yes'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.removeMealInfo();
      }
    });
  }

  discardMealInfo() {
    if (this.mealDetail.id) {
      this.mealDetail.editable = false;
      this.resetInputValue();
    } else {
      this.deleted.emit(this.mealDetail);
    }
  }

  saveMealInfo() {
    this.mealDetail.name = this.nameFormControl.value;
    this.mealDetail.description = this.descriptionFormControl.value;
    this.mealDetail.price = this.refPriceFormControl.value;
    this.mealDetail.calories = this.caloriesFormControl.value;
    this.uploadMealInfo().subscribe(result => {
      this.mealDetail.editable = false;
      this.mealDetail.id = result.id;
      this.oldPictureId = this.mealDetail.pictureId;
      this.oldPictureUrl = this.mealDetail.pictureUrl;
      this.showSnackMessage(result.message);
    }, error => {
      this.errorToDisplay(Url.getErrorMessage(error));
    });
  }

  removeMealInfo() {
    if (this.mealDetail.id) {
      this.deleteMealInfo().subscribe(result => {
        this.deleted.emit(this.mealDetail);
        this.showSnackMessage(result.message);
      }, error => {
        this.showSnackMessage(error.error.message);
      });
    } else {
      this.deleted.emit(this.mealDetail);
    }
  }

  onFileChange(fileInput) {
    const selectedFile = fileInput.target.files[0];
    if (selectedFile == null) {
      return;
    }
    this.fileUploading = true;
    this.pictureHandler.uploadPicture(selectedFile).then((httpPosted: Observable<any>) => {
      httpPosted.subscribe((event) => this.fileUploadingEvent(event), (error) => this.fileReceiveError(error));
    }).catch((error) => this.fileReceiveError(error));
  }

  private fileUploadingEvent(event) {
    if (event.type === HttpEventType.UploadProgress) {
      this.fileProgress = 100 * event.loaded / event.total;
    } else if (event.body && event.body.id) {
      this.mealDetail.pictureId = event.body.id;
      this.mealDetail.pictureUrl = event.body.url;
      this.fileUploading = false;
    }
  }

  private fileReceiveError(error) {
    this.dialog.open(ConfirmComponent, {
      width: '300px',
      data: {
        header: 'Error received!',
        body: error.error.message || error.message,
        action: 'OK'
      }
    });
  }

  errorToDisplay(error: any) {
    if (error.code === 300) { // custom error message
      error.message.forEach((element: { type: string; message: string; }) => {
        if (element.type === 'popup') {
          this.showSnackMessage('error.error.message || error.message');
          return;
        }
        this.serverErrorMessage[element.type] = element.message;
        this.formData.get(element.type).setErrors({ serverInvalid: true });
        this.formData.get(element.type).markAsTouched();
      });
    } else {
      this.showSnackMessage(error.error.message || error.message);
    }
  }

  updateIngredient(ingredientIndex: number) {
    this.mealDetail.ingredients[ingredientIndex].name = this.ingredientsFormArray.at(ingredientIndex).get('name').value;
    this.mealDetail.ingredients[ingredientIndex].quantity = this.ingredientsFormArray.at(ingredientIndex).get('quantity').value;
    this.mealDetail.ingredients[ingredientIndex].unit = this.ingredientsFormArray.at(ingredientIndex).get('unit').value;
  }

  removeIngredient(ingredientIndex: number) {
    this.mealDetail.ingredients.splice(ingredientIndex, 1);
    this.ingredientsFormArray.removeAt(ingredientIndex);
    this.table.renderRows();
  }

  addIngredientRow() {
    this.mealDetail.ingredients.push(new Ingredient());
    this.ingredientsFormArray.push(this.getIngredientFormGroup());
    this.table.renderRows();
  }

  getIngredientFormGroup(): FormGroup {
    const nameFC = new FormControl('', Validators.required);
    const quantityFC = new FormControl(0, [
      Validators.required,
      Validators.min(0.001)
    ]);
    const unitFC = new FormControl('');
    const formGroup = new FormGroup({
      name: nameFC,
      quantity: quantityFC,
      unit: unitFC
    });
    return formGroup;
  }

  private uploadMealInfo(): Observable<any> {
    return this.vendorMealHandler.uploadMealInfo(this.mealDetail);
  }
  private deleteMealInfo(): Observable<any> {
    return this.vendorMealHandler.deleteMealInfo(this.mealDetail);
  }

  private resetInputValue() {
    this.nameFormControl.setValue(this.mealDetail.name);
    this.descriptionFormControl.setValue(this.mealDetail.description);
    this.refPriceFormControl.setValue(this.mealDetail.price);
    this.caloriesFormControl.setValue(this.mealDetail.calories);
    this.mealDetail.pictureId = this.oldPictureId;
    this.mealDetail.pictureUrl = this.oldPictureUrl;
    this.resetIngredients();
  }

  private copyIngredients() {
    this.oldIngredients = this.mealDetail.ingredients.map((ingredient) => {
      return new Ingredient(ingredient.name, ingredient.quantity, ingredient.unit);
    });
  }

  private resetIngredients() {
    this.mealDetail.ingredients = this.oldIngredients.map((ingredient) => {
      return new Ingredient(ingredient.name, ingredient.quantity, ingredient.unit);
    });
    this.mealDetail.ingredients.forEach((ingredient: Ingredient, index: number) => {
      this.ingredientsFormArray.at(index).get('name').setValue(ingredient.name);
      this.ingredientsFormArray.at(index).get('quantity').setValue(ingredient.quantity);
      this.ingredientsFormArray.at(index).get('unit').setValue(ingredient.unit);
    });
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
