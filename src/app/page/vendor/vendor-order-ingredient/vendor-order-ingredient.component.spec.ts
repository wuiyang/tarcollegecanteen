import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorOrderIngredientComponent } from './vendor-order-ingredient.component';

describe('VendorOrderIngredientComponent', () => {
  let component: VendorOrderIngredientComponent;
  let fixture: ComponentFixture<VendorOrderIngredientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorOrderIngredientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorOrderIngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
