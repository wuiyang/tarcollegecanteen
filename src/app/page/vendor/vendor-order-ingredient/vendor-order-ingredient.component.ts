import { Component, OnInit, Input } from '@angular/core';
import { Menu } from 'src/app/models/menu.model';
import { Meal } from 'src/app/models/meal.model';

@Component({
  selector: 'app-vendor-order-ingredient',
  templateUrl: './vendor-order-ingredient.component.html',
  styleUrls: ['./vendor-order-ingredient.component.css']
})
export class VendorOrderIngredientComponent implements OnInit {
  @Input() menu: Menu;
  @Input() mealOrderQuantity: any;
  showData: Meal[] = [];
  displayedColumns: string[] = ['name', 'unitamount', 'quantity', 'subtotal'];

  constructor() { }

  ngOnInit() {
    this.showData = this.menu.meals.filter((meal: Meal) => this.mealOrderQuantity[meal.id]);
  }

  toDisplayNumber(num: number) {
    return parseFloat(num.toFixed(4));
  }

}
