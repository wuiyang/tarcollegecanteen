import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMealComponent } from './vendor-meal.component';

describe('VendorMealComponent', () => {
  let component: VendorMealComponent;
  let fixture: ComponentFixture<VendorMealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
