import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Meal } from 'src/app/models/meal.model';
import { VendorMealService } from 'src/app/services/vendor/vendor-meal.service';
import { MatSnackBar } from '@angular/material';
import { ScrollToBottomService } from 'src/app/services/page/scroll-to-bottom.service';

@Component({
  selector: 'app-vendor-meal',
  templateUrl: './vendor-meal.component.html',
  styleUrls: ['./vendor-meal.component.css']
})
export class VendorMealComponent implements OnInit, AfterViewChecked {
  mealList: Array<Meal> = [];
  loaded = false;
  scrollToBottom = false;

  constructor(private vendorMealHandler: VendorMealService, private scrollService: ScrollToBottomService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getAllMeals();
  }

  ngAfterViewChecked() {
    if (this.scrollToBottom) {
      this.scrollService.scrollToBottom();
      this.scrollToBottom = false;
    }
  }

  newVendorMealDetail() {
    const tempMeal: Meal = new Meal('');
    tempMeal.editable = true;
    this.mealList.push(tempMeal);
    this.scrollToBottom = true;
  }

  getAllMeals() {
    this.vendorMealHandler.getMealList().subscribe(mealList => {
      this.mealList = mealList;
      this.loaded = true;
    }, error => {
      this.showSnackMessage(error.message);
      this.loaded = true;
    });
  }

  onDeleted(meal: Meal) {
    const index = this.mealList.indexOf(meal);
    if (index !== -1) {
      this.mealList.splice(index, 1);
    }
  }

  private showSnackMessage(message: string) {
    this.snackBar.open(message, 'Dismiss', { duration: 2000 });
  }
}
// https://stackoverflow.com/questions/40678206/angular-2-filter-search-list
