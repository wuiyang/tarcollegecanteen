# TODO
credit point transaction/history (2/4) [Done]
breakfast and lunch (3/4) [done]
order (4/4) [done]
COUPONS (6/4) [done]
ingredient (7/4) [done]
order report (8/4) [done]
summary report (8/4) [done]
ingredient report (9/4) [done]
verify coupons (9/4) [done]
consumption report (10/4) [done]
canteen operator (?/?)
invoice (12/4)
account modification (9/4)
feedback (?/4)
feedback reply (?/4)

# Canteen

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
